using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class King : MonoBehaviour
{
    public int _spawnedLeft = 254 - 161;
    private float _attackTimer = 0f;
    private bool _isDamagable = false;
    public int _healthPoints = 15;
    private float _damageTimer = 0f;
    private float _damageTimeLimit = 1f;

    [SerializeField] private GameObject LooterPrefab;
    [SerializeField] private GameObject BugPrefab;
    [SerializeField] private GameObject Player;
    [SerializeField] private GameObject Shield;
    private BugLog BugLog;

    public Animator Animator;

    // Start is called before the first frame update
    void Start()
    {
        BugLog = GameObject.FindWithTag("BugLog").GetComponent<BugLog>();
    }

    // Update is called once per frame
    void Update()
    {
        _attackTimer += Time.deltaTime;
        if (BugLog.isBugged("King") && _spawnedLeft > 0 && _attackTimer > 0.5f)
        {
            Attack();
            _spawnedLeft--;
            _attackTimer = 0f;
        }
        if (_spawnedLeft <= 0)
        {
            Shield.SetActive(false);
            _isDamagable = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerAttack")
        {
            _damageTimer = 1f;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "PlayerAttack")
        {
            if (_isDamagable)
            {
                _damageTimer += Time.fixedDeltaTime;
                if (_damageTimer >= _damageTimeLimit)
                {
                    TakeDamage(collision.gameObject.GetComponentInParent<Player>().GetDealtDamage());
                    _damageTimer = 0f;
                }
            }
        }
    }

    private void Attack()
    {
        Looter newLooter = Instantiate(LooterPrefab, transform.position + new Vector3(-1, 0, 0), Quaternion.identity).GetComponent<Looter>();
        newLooter.transform.SetParent(transform);
        newLooter._isFollowing = true;
    }


    public void TakeDamage(int value)
    {
        Animator.SetTrigger("isDamaged");
        SoundManager.PlaySound("damage" + Mathf.Min(1, (BugLog.isBugged("Looter") ? 1 : 0)));
        _healthPoints -= Math.Min(_healthPoints, value);
        if (_healthPoints == 0)
        {
            OnDeath();
        }
    }
    private void OnDeath()
    {
        Instantiate(BugPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}

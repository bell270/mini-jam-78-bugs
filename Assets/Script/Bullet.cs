using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Rigidbody2D _rb;
    private float _speed = 10f;
    private float _direction = 1f;
    private int _damage = 0;
    private int _bulletPower = 2500;
    private bool _init = true;
    [SerializeField] private GameObject CloudPrefab;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _init = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (_init)
        {
            _rb.AddForce((new Vector2(_direction, 0f)).normalized * _speed * Time.fixedDeltaTime, ForceMode2D.Impulse);
            _init = false;
        }
    }

    public void SetSpeed(float speed) => _speed = speed;

    public void SetDirection(float direction) => _direction = direction;
    public void SetPower(int power) => _bulletPower = power;

    public Vector2 GetVelocity()
    {
        return _rb.velocity;
    }
    public void SetGravity(bool isActive)
    {
        GetComponent<Rigidbody2D>().gravityScale = isActive ? 1 : 0;
    }

    public void SetDamage(int damage) => _damage = damage;

    public int GetDamage() => _damage;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject cloud = Instantiate(CloudPrefab, transform.position, Quaternion.identity);
        cloud.transform.SetParent(transform.parent);
        Rigidbody2D rb = collision.collider.GetComponent<Rigidbody2D>();
        if (rb != null)
        {
            rb.velocity = Vector2.zero;
            rb.AddForce(_bulletPower * new Vector2(_direction,0));
        }
        Destroy(gameObject);
    }
}

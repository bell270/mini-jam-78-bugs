using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class BugLog : MonoBehaviour
{
    [SerializeField] private int _bugCount = 0;
    [SerializeField] private int _bugLevel = 0;

    [SerializeField] public UI_Text BugText;
    [SerializeField] public Image HP;
    [SerializeField] public Image Shooter;
    [SerializeField] public Image Looter;
    [SerializeField] public Image Jump;
    [SerializeField] public Image Tile;
    [SerializeField] public Image Flight;
    [SerializeField] public Image Bug;

    public Tilemap GroundTilemap;

    [SerializeField] public TileBase tile1;
    [SerializeField] public TileBase tile1Glitch;
    [SerializeField] public TileBase tile2;
    [SerializeField] public TileBase tile2Glitch;
    [SerializeField] public TileBase tile3;
    [SerializeField] public TileBase tile3Glitch;
    [SerializeField] public TileBase tile4;
    [SerializeField] public TileBase tile4Glitch;

    public LayerMask PlayerLayer;
    public LayerMask GroundLayer;

    public PlayableDirector startTimeline;
    public PlayableDirector endTimeline;

    // Start is called before the first frame update
    void Start()
    {
        startTimeline.Play();

        BugText.SetText(_bugCount + " / 255");

        HP.color = new Color(1, 1, 1, isBugged("HP") ? 1 : 0);
        Shooter.color = new Color(1, 1, 1, isBugged("Shooter") ? 1 : 0);
        Looter.color = new Color(1, 1, 1, isBugged("Looter") ? 1 : 0);
        Jump.color = new Color(1, 1, 1, isBugged("Jump") ? 1 : 0);
        Tile.color = new Color(1, 1, 1, isBugged("Collider") ? 1 : 0);
        Flight.color = new Color(1, 1, 1, isBugged("Flight") ? 1 : 0);
        Bug.color = new Color(1, 1, 1, isBugged("Bug") ? 1 : 0);
    }

    private void Update()
    {

    }

    private bool UpdateLevel()
    {
        int prevLevel = _bugLevel;
        if (_bugCount >= 128)
        {
            _bugLevel = 4;
        }
        else if (_bugCount >= 100)
        {
            _bugLevel = 3;
        }
        else if (_bugCount >= 64)
        {
            _bugLevel = 2;
        }
        else if (_bugCount >= 16)
        {
            _bugLevel = 1;
        }
        else
        {
            _bugLevel = 0;
        }

        if (isBugged("Collider"))
        {
            Physics2D.IgnoreLayerCollision((int)Mathf.Log(PlayerLayer, 2), (int)Mathf.Log(GroundLayer, 2), true);
            GroundTilemap.SwapTile(tile1, tile1Glitch);
            GroundTilemap.SwapTile(tile2, tile2Glitch);
            GroundTilemap.SwapTile(tile3, tile3Glitch);
            GroundTilemap.SwapTile(tile4, tile4Glitch);
        }

        HP.color = new Color(1, 1, 1, isBugged("HP") ? 1 : 0);
        Shooter.color = new Color(1, 1, 1, isBugged("Shooter") ? 1 : 0);
        Looter.color = new Color(1, 1, 1, isBugged("Looter") ? 1 : 0);
        Jump.color = new Color(1, 1, 1, isBugged("Jump") ? 1 : 0);
        Tile.color = new Color(1, 1, 1, isBugged("Collider") ? 1 : 0);
        Flight.color = new Color(1, 1, 1, isBugged("Flight") ? 1 : 0);
        Bug.color = new Color(1, 1, 1, isBugged("Bug") ? 1 : 0);

        if(isBugged("Bug"))
        {
            BugText.SetColor(Color.red);
            endTimeline.Play();
        }
        return !(prevLevel == _bugLevel);
    }

    public bool AddBug(int increment)
    {
        _bugCount += increment;
        BugText.SetText(_bugCount + " / 255");
    /*+ (BugLog.GetBugCount() >= 2 ? "Looters; " : "")
    + (BugLog.GetBugCount() >= 3 ? "Shooters; " : "")
    + (BugLog.GetBugCount() >= 15 ? "YOU; " : "")
    );*/
        return UpdateLevel();
    }

    public int GetBugCount()
    {
        return _bugCount;
    }

    public int GetBugLevel()
    {
        return _bugLevel;
    }

    public bool isBugged(string type)
    {
        bool result = false;
        switch (type)
        {
            case "HP":
                return _bugCount > 0;
                break;
            case "Looter":
                return _bugLevel >= 2 && (_bugCount < 161 || _bugCount > 253);
                break;
            case "Shooter":
                return _bugLevel >= 1;
                break;
            case "Jump":
                return _bugLevel >= 3;
                break;
            case "Collider":
                return _bugLevel >= 4;
                break;
            case "Flight":
                return _bugCount >= 160;
                break;
            case "Autopilot":
                return _bugCount > 255;
                break;
            case "Bug":
                return _bugCount > 255;
                break;
            case "King":
                return _bugCount >= 161;
                break;
            default:
                return false;
                break;
        }
        return result;
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Princess : MonoBehaviour
{
    [SerializeField] private int _healthPoints = 15;
    private float _damageTimer = 0f;
    private float _damageTimeLimit = 1.5f;
    private bool _isPanicking = true;

    [SerializeField] private float _speed = 4f;
    private float _moveTimer;
    private Rigidbody2D _rb;

    [SerializeField] private Sprite _sadSprite;
    [SerializeField] private GameObject King;
    [SerializeField] private GameObject BugPrefab;
    private Vector3 _movement;

    public Animator Animator;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _movement = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        Animator.SetFloat("Speed", _rb.velocity.magnitude);

        Vector3 localScale = transform.localScale;
        float newScaleX = (_movement.x == 0 ? localScale.x : Math.Abs(localScale.x)) * (_movement.x >= 0 ? 1 : -1);
        transform.localScale = new Vector3(newScaleX, localScale.y, localScale.z);
        
        if(King.GetComponent<King>()._healthPoints < 10)
        {
            GetComponent<SpriteRenderer>().sprite = _sadSprite;
            Animator.SetBool("isSad", true);
        }
    }

    private void FixedUpdate()
    {
        _moveTimer += Time.fixedDeltaTime;
        _rb.MovePosition(_rb.position + new Vector2(_movement.x, _movement.y) * Time.fixedDeltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerAttack")
        {
            _damageTimer = 1f;
        }
        if (_isPanicking && collision.tag == "Player")
        {
            _movement = (transform.position - collision.transform.position).normalized * _speed;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "PlayerAttack")
        {
            _damageTimer += Time.fixedDeltaTime;
            if (_damageTimer >= _damageTimeLimit)
            {
                TakeDamage(collision.gameObject.GetComponentInParent<Player>().GetDealtDamage());
                _damageTimer = 0f;
            }
        }
        if (_isPanicking && collision.tag == "Player")
        {
            _movement = Vector3.Project((transform.position - collision.transform.position), new Vector3(1, 0, 0)).normalized * _speed;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            _movement = Vector3.zero;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.GetComponent<Player>() != null)
        {
            Rigidbody2D rb = collision.collider.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                rb.velocity = Vector2.zero;
                rb.AddForce(5000 * new Vector2(-1, 0));
            }
        }
    }

    public void TakeDamage(int value)
    {
        Animator.SetTrigger("isDamaged");
        SoundManager.PlaySound("damage" + 0);
        _healthPoints -= Math.Min(_healthPoints, value);
        if (_healthPoints == 0)
        {
            OnDeath();
        }
    }

    public void SetHealth(int value)
    {
        _healthPoints = value;
    }

    private void OnDeath()
    {
        Instantiate(BugPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    public void Panic()
    {

    }
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DamageIndicator : MonoBehaviour
{
    private float _fadeTimer = 0f;
    private float _fadeTimerLimit = 2f;
    private bool _isHidden = true;
    public TextMeshPro textbox;
    void Start()
    {
        _fadeTimer = 0f;
        textbox.alpha = 0f;
        textbox = GetComponent<TextMeshPro>();
    }

    private void Update()
    {
        float parentTransformX = Mathf.Sign(GetComponentInParent<Transform>().transform.localScale.x);
        transform.localScale = new Vector3((parentTransformX * transform.localScale.x), transform.localScale.y, transform.localScale.z);
        if (_isHidden && _fadeTimer >= 0f)
        {
            textbox.alpha = (_fadeTimer / _fadeTimerLimit);
            _fadeTimer -= Time.deltaTime;
        }

    }
    public void SetText(string text)
    {
        textbox.text = text;
    }

    public void SetTextAndTimer(string text)
    {
        textbox.text = text;
        _fadeTimer = _fadeTimerLimit;
        _isHidden = true;
    }
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UI_Text : MonoBehaviour
{
    private float _fadeTimer = 0f;
    private float _fadeTimerLimit = 3f;
    private bool _isHidden = false;
    public TextMeshProUGUI textbox;
    void Start()
    {
        _fadeTimer = 0f;
    }

    private void Update()
    {
        if (_isHidden && _fadeTimer >= 0f)
        {
            textbox.alpha = (_fadeTimer / _fadeTimerLimit);
            _fadeTimer -= Time.deltaTime;
        }

    }
    public void SetText(string text)
    {
        textbox.text = text;
    }
    public void SetColor(Color color)
    {
        textbox.color = color;
    }

    public void SetTextAndTimer(string text)
    {
        textbox.text = text;
        _fadeTimer = _fadeTimerLimit;
        _isHidden = true;
    }
}

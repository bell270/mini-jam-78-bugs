using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bug : MonoBehaviour
{
    private BugLog BugLog;
    private Vector2 _movement = new Vector2(0, 0);
    private float _speed = 0.5f;
    private Rigidbody2D _rb;
    public bool pickupDone = false;

    private void Start()
    {
        BugLog = GameObject.FindWithTag("BugLog").GetComponent<BugLog>();
        _rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        _rb.MovePosition(_rb.position + _movement * _speed);
    }

    private void FixedUpdate()
    {
        _rb.MovePosition(_rb.position + _movement * _speed * Time.fixedDeltaTime);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Player player = collision.collider.GetComponent<Player>();
        if (player != null)
        {
            if (!pickupDone)
            {
                GetComponent<BoxCollider2D>().enabled = false;
                bool levelup = BugLog.AddBug(1);
                SoundManager.PlaySound(levelup ? "levelup" : ("pickup" + Mathf.Min(3, BugLog.GetBugLevel())));
                Destroy(gameObject);
                pickupDone = true;
                Invoke("ResetPickup", 0.2f);
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            _movement = (collision.transform.position - transform.position).normalized;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            _movement = Vector2.zero;
        }
    }

    void ResetPickup()
    {
        pickupDone = false;
    }
}

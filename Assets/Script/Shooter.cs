using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField] private int _healthPoints = 5;
    private float _damageTimer = 0f;
    private float _damageTimeLimit = 1.5f;
    [SerializeField] private bool _isPanicking = false;
    [SerializeField] private bool _isFleeing = false;

    private Rigidbody2D _rb;
    private Vector3 _movement = Vector3.zero;
    [SerializeField] private float _speed = 1f;

    [SerializeField] private float _shootRate;
    [SerializeField] private float _shootSpeed;
    [SerializeField] private float _shootDirection;
    [SerializeField] private float _halfSize = 0.6f;
    [SerializeField] private bool _shootToKill;
    private float _shootTimer;
    [SerializeField] private GameObject BulletPrefab;

    [SerializeField] private GameObject BugPrefab;
    private BugLog BugLog;

    public Animator Animator;

    // Start is called before the first frame update
    void Start()
    {
        BugLog = GameObject.FindWithTag("BugLog").GetComponent<BugLog>();
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        bool isBugged = BugLog.isBugged("Shooter");
        _healthPoints = Math.Min(_healthPoints, (isBugged ? 0 : _healthPoints));
        _shootToKill = !isBugged;
        _damageTimeLimit = isBugged ? _damageTimeLimit : 0.5f;
        Animator.SetBool("isBugged", isBugged);
    }

    private void FixedUpdate()
    {
        if (_isFleeing)
        {
            _rb.MovePosition(_rb.position + new Vector2(_movement.x, _movement.y) * Time.fixedDeltaTime);
        }
        else
        {
            _shootTimer += Time.fixedDeltaTime;
            if (_shootTimer > 1f / _shootRate)
            {
                Vector3 _gunPoint = new Vector3(transform.position.x + _shootDirection * _halfSize, transform.position.y, transform.position.z);
                GameObject newBullet = Instantiate(BulletPrefab, _gunPoint, Quaternion.identity);
                newBullet.transform.SetParent(transform);
                newBullet.GetComponent<Bullet>().SetDirection(_shootDirection);
                newBullet.GetComponent<Bullet>().SetSpeed(_shootSpeed);
                newBullet.GetComponent<Bullet>().SetDamage(_shootToKill ? 10 : 0);
                newBullet.GetComponent<Bullet>().SetGravity(BugLog.isBugged("Shooter"));
                newBullet.GetComponent<Bullet>().SetPower(BugLog.isBugged("Shooter") ? 0 : 2500 );
                _shootTimer = 0;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerAttack")
        {
            _damageTimer = 1f;
        }
        if (_isPanicking && collision.tag == "Player")
        {
            _movement = (transform.position - collision.transform.position).normalized * _speed;
            _isFleeing = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "PlayerAttack")
        {
            _damageTimer += Time.fixedDeltaTime;
            if (_damageTimer >= _damageTimeLimit)
            {
                TakeDamage(collision.gameObject.GetComponentInParent<Player>().GetDealtDamage());
                _damageTimer = 0f;
            }
        }
        if (_isPanicking && collision.tag == "Player")
        {
            _movement = (transform.position - collision.transform.position).normalized * _speed;
            _isFleeing = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            _movement = Vector3.zero;
            _isFleeing = false;
        }
    }

    public bool ShootToKill { get => _shootToKill; set => _shootToKill = value; }

    public float GetShootDirection()
    {
        return _shootDirection;
    }

    public void SetShootDirection(float value)
    {
        _shootDirection = value;
    }

    public float ShootRate { get => _shootRate; set => _shootRate = value; }
    public float ShootSpeed { get => _shootSpeed; set => _shootSpeed = value; }

    public void TakeDamage(int value)
    {
        SoundManager.PlaySound("damage" + Mathf.Min(1, (BugLog.isBugged("Shooter") ? 1 : 0)));
        Animator.SetTrigger("isDamaged");
        _healthPoints -= Math.Min(_healthPoints, value);
        if (_healthPoints == 0)
        {
            OnDeath();
        }
    }

    public void SetHealth(int value)
    {
        _healthPoints = value;
    }

    private void OnDeath()
    {
        Instantiate(BugPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}

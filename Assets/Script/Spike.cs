using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour
{
    public Sprite sprite1;
    public Sprite sprite2;
    private List<Sprite> sprites = new List<Sprite>();
    // Start is called before the first frame update
    void Start()
    {
        sprites.Add(sprite1);
        sprites.Add(sprite2);
        Sprite curSprite = sprites[Random.Range(0, 2)];
        GetComponent<SpriteRenderer>().sprite = curSprite;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

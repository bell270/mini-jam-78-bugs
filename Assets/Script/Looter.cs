using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Looter : MonoBehaviour
{
    [SerializeField] private int _healthPoints = 5;
    private float _damageTimer = 0f;
    private float _damageTimeLimit = 1.5f;
    [SerializeField] public bool _isPanicking = false;
    [SerializeField] private bool _isFleeing = false;
    [SerializeField] public bool _isFollowing = false;

    private Rigidbody2D _rb;
    private Vector3 _movement;
    [SerializeField] private float _speed = 1f;
    private float _moveTimer = 0f;
    private LineRenderer _trackRenderer;
    private List<Vector3> _positions = new List<Vector3>();
    private int _nextPositionIdx = 0;
    private Vector3 _nextPosition;
    [SerializeField] private bool _showTrack = false;

    [SerializeField] private GameObject BugPrefab;
    [SerializeField] private GameObject Player;
    private BugLog BugLog;

    public Animator Animator;

    // Start is called before the first frame update
    void Start()
    {
        BugLog = GameObject.FindWithTag("BugLog").GetComponent<BugLog>();
        Player = GameObject.FindWithTag("Player");

        _trackRenderer = GetComponentInChildren<LineRenderer>().gameObject.GetComponent<LineRenderer>();
        _trackRenderer.enabled = false;
        Vector3[] tmpPositions = new Vector3[_trackRenderer.positionCount];
        _trackRenderer.GetPositions(tmpPositions);
        for(int i = 0; i < _trackRenderer.positionCount; i++)
        {
            /*_positions.Add(transform.TransformPoint(tmpPositions[i]));*/
            _positions.Add(tmpPositions[i]);
        }
        _nextPositionIdx = -1;
        GetNextPosition();

        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_isFollowing)
        {
            bool isBugged = BugLog.isBugged("Looter");
            _damageTimeLimit = isBugged ? _damageTimeLimit : 0.5f;
            _trackRenderer.enabled = isBugged || _showTrack;
            _healthPoints = Math.Min(_healthPoints, (isBugged ? 0 : _healthPoints));
            Animator.SetBool("isBugged", isBugged);

            Vector3 localScale = transform.localScale;
            transform.localScale = new Vector3(Math.Abs(localScale.x) * (_movement.x > 0 ? 1 : -1), localScale.y, localScale.z);


            if (!_isFleeing)
            {
                if (Vector3.Distance(transform.position, _nextPosition) < 0.2f)
                {
                    GetNextPosition();
                }
                _movement = (_nextPosition - transform.position).normalized * 2 *_speed;
            }
        }
        else
        {
            _movement = (Player.transform.position - transform.position).normalized * 3 * _speed;
        }
    }

    private void FixedUpdate()
    {
        if (!BugLog.isBugged("Looter") || _isFleeing || _isFollowing)
        {
            _rb.MovePosition(_rb.position + new Vector2(_movement.x, _movement.y) * Time.fixedDeltaTime);
        }
        else
        {
            _moveTimer += Time.fixedDeltaTime;
            if(_moveTimer >= 2f)
            {
                _rb.position = new Vector2(_nextPosition.x, _nextPosition.y);
                _rb.velocity = new Vector2(0, 0);
                _moveTimer = 0f;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerAttack")
        {
            _damageTimer = 1f;
        }
        if (_isPanicking && collision.tag == "Player")
        {
            _movement = (transform.position - collision.transform.position).normalized * _speed;
            _isFleeing = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "PlayerAttack")
        {
            _damageTimer += Time.fixedDeltaTime;
            if (_damageTimer >= _damageTimeLimit)
            {
                TakeDamage(collision.gameObject.GetComponentInParent<Player>().GetDealtDamage());
                _damageTimer = 0f;
            }
        }
        if (_isPanicking && collision.tag == "Player")
        {
            _movement = (transform.position - collision.transform.position).normalized * _speed;
            _isFleeing = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            _movement = Vector3.zero;
            _isFleeing = false;
        }
    }

    private void GetNextPosition()
    {
        _nextPositionIdx++;
        if (_nextPositionIdx >= _positions.Count)
        {
            _nextPositionIdx = 0;
        }
        _nextPosition = _positions[_nextPositionIdx];
    }

    public void TakeDamage(int value)
    {
        Animator.SetTrigger("isDamaged");
        SoundManager.PlaySound("damage" + Mathf.Min(1, (BugLog.isBugged("Looter") ? 1 : 0)));
        _healthPoints -= Math.Min(_healthPoints, value);
        if (_healthPoints == 0)
        {
            OnDeath();
        }
    }

    public void SetHealth(int value)
    {
        _healthPoints = value;
    }

    private void OnDeath()
    {
        Instantiate(BugPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}

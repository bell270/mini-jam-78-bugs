using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public static int totalAmount;
    [SerializeField] public int _spawnType;
    private LineRenderer _lrForLooter;

    [SerializeField] private GameObject ShooterPrefab;
    [SerializeField] private GameObject LooterPrefab;
    [SerializeField] private GameObject RunnerPrefab;

    [SerializeField] private float _spawnRadius = 20f;
    [SerializeField] private float _directionForShooter;
    [SerializeField] private bool _spawned = false;
    [SerializeField] private bool _isPanicking = false;

    // Start is called before the first frame update
    void Start()
    {
        totalAmount++;
        _lrForLooter = GetComponent<LineRenderer>();
        foreach(Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }

    // private void OnBecameVisible()
    // {
    //     Spawn();
    // }

    public void Spawn()
    {
        GameObject UnitPrefab;
        if (_spawnType == 0)
        {
            UnitPrefab = ShooterPrefab;
        }
        else if (_spawnType == 1)
        {
            UnitPrefab = LooterPrefab;
        }
        else
        {
            UnitPrefab = RunnerPrefab;
        }
        GameObject newUnit = Instantiate(UnitPrefab, transform.position, Quaternion.identity);
        newUnit.transform.SetParent(transform.parent);
        if (_spawnType == 0)
        {
            newUnit.GetComponent<Shooter>().SetShootDirection(_directionForShooter);
            Vector3 localScale = newUnit.GetComponent<Shooter>().transform.localScale;
            newUnit.GetComponent<Shooter>().transform.localScale = new Vector3((_directionForShooter > 0 ? 1 : -1) * localScale.x, localScale.y, localScale.z);
        }
        else if (_spawnType == 1)
        {
            Vector3[] newPositions = new Vector3[_lrForLooter.positionCount];
            _lrForLooter.GetPositions(newPositions);
            newUnit.GetComponentInChildren<LineRenderer>().positionCount = _lrForLooter.positionCount;
            newUnit.GetComponentInChildren<LineRenderer>().SetPositions(newPositions);
            newUnit.GetComponent<Looter>()._isPanicking = _isPanicking;
        }
        else
        {
            UnitPrefab = RunnerPrefab;
        }
        Destroy(gameObject);
    }

    public float GetSpawnRadius()
    {
        return _spawnRadius;
    }

    public float GetTotal()
    {
        return totalAmount;
    }
}

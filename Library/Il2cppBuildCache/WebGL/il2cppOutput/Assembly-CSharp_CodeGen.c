﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Bug::Start()
extern void Bug_Start_mC486A11B805E5F181C57FB7FC1908E308176C4C4 (void);
// 0x00000002 System.Void Bug::Update()
extern void Bug_Update_mB67BCAA4E66B107438CF06A3FE3D412041B8ED3B (void);
// 0x00000003 System.Void Bug::FixedUpdate()
extern void Bug_FixedUpdate_m47FFFA9E2C5A90A79C23C2A87DF32453F46B40E0 (void);
// 0x00000004 System.Void Bug::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Bug_OnCollisionEnter2D_m2CE3B919EABFD4E6BA5D00235F9C5918A64240AF (void);
// 0x00000005 System.Void Bug::OnTriggerStay2D(UnityEngine.Collider2D)
extern void Bug_OnTriggerStay2D_mCEBBF08F6B8F4FD7340C9A1FD3E152E25C09D2A8 (void);
// 0x00000006 System.Void Bug::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Bug_OnTriggerExit2D_m7792D0B1D809DD72911CD40056458B2F647154FD (void);
// 0x00000007 System.Void Bug::ResetPickup()
extern void Bug_ResetPickup_m314CB1A7C7B893D01CF52559CBBDE89E054A0810 (void);
// 0x00000008 System.Void Bug::.ctor()
extern void Bug__ctor_mAEFF79F71DBF4B6C9BFFF1D2DABB336DB6110F57 (void);
// 0x00000009 System.Void BugLog::Start()
extern void BugLog_Start_mB8448229845ED2D12A630EA233192B27E55C20D8 (void);
// 0x0000000A System.Void BugLog::Update()
extern void BugLog_Update_mFEFE813679327D37E4B5F4202F092013365CADB0 (void);
// 0x0000000B System.Boolean BugLog::UpdateLevel()
extern void BugLog_UpdateLevel_m63BED2555F63579FC3024C41669AEE1E3CEB1EB9 (void);
// 0x0000000C System.Boolean BugLog::AddBug(System.Int32)
extern void BugLog_AddBug_m05179868B25C0F7A32AB9DC44B2576DB528C7341 (void);
// 0x0000000D System.Int32 BugLog::GetBugCount()
extern void BugLog_GetBugCount_m17ADF86EF09DE45B5EE744455BAD25908CAF21B2 (void);
// 0x0000000E System.Int32 BugLog::GetBugLevel()
extern void BugLog_GetBugLevel_m88AC8D9DB1FA2EE1283CD86DA2A980A799362FEF (void);
// 0x0000000F System.Boolean BugLog::isBugged(System.String)
extern void BugLog_isBugged_mCDF827C888D32AEE4CFB53936E22BE6E07D5BDE0 (void);
// 0x00000010 System.Void BugLog::.ctor()
extern void BugLog__ctor_m9C7EA3327E58B41B2F1F5E021123F6962D60A309 (void);
// 0x00000011 System.Void Bullet::Start()
extern void Bullet_Start_m58181B46F80FE1ABECE1AFF455C253B51B7EB0E4 (void);
// 0x00000012 System.Void Bullet::Update()
extern void Bullet_Update_mB82408CA535D8533168045E7C0321090448B596B (void);
// 0x00000013 System.Void Bullet::FixedUpdate()
extern void Bullet_FixedUpdate_m2CED3258420C0BB71A2CE12F3E0C0287A8D72145 (void);
// 0x00000014 System.Void Bullet::SetSpeed(System.Single)
extern void Bullet_SetSpeed_m50BFFC7C609F4ACD0A9CA122DF5D47A039223932 (void);
// 0x00000015 System.Void Bullet::SetDirection(System.Single)
extern void Bullet_SetDirection_m04213FA5AABE357B0C567FA2F5AB5DC3FEE4BCAF (void);
// 0x00000016 System.Void Bullet::SetPower(System.Int32)
extern void Bullet_SetPower_m2ED5613FFE53994BAA2E7F2DDDA7982063170384 (void);
// 0x00000017 UnityEngine.Vector2 Bullet::GetVelocity()
extern void Bullet_GetVelocity_m68CC73C2858FC7F552CF400321CFDD49E04C991F (void);
// 0x00000018 System.Void Bullet::SetGravity(System.Boolean)
extern void Bullet_SetGravity_m478DCD844E2E22F610B65A2A8B4D0D588BFE4F64 (void);
// 0x00000019 System.Void Bullet::SetDamage(System.Int32)
extern void Bullet_SetDamage_m6A6975C68C1F685D519FF14644FCB9B10B4FF906 (void);
// 0x0000001A System.Int32 Bullet::GetDamage()
extern void Bullet_GetDamage_m9CA04D892B75A0D99E7A8AFEA83D34FCA1487599 (void);
// 0x0000001B System.Void Bullet::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Bullet_OnCollisionEnter2D_m4CC80D190EE3A6B6E2EDA524A121942D0285D0A9 (void);
// 0x0000001C System.Void Bullet::.ctor()
extern void Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC (void);
// 0x0000001D System.Void CharacterController2D::Awake()
extern void CharacterController2D_Awake_mFD21C000EE8097373F734648EAC2BFDB901197FA (void);
// 0x0000001E System.Void CharacterController2D::FixedUpdate()
extern void CharacterController2D_FixedUpdate_m00D8CBF2991777D0875EB1D13AF8222831A2F0E8 (void);
// 0x0000001F System.Void CharacterController2D::Move(System.Single,System.Boolean,System.Boolean)
extern void CharacterController2D_Move_mE541579D7B4A796A34C1E2AFF0DAD5DEADD475F9 (void);
// 0x00000020 System.Void CharacterController2D::Flip()
extern void CharacterController2D_Flip_m8F1D0D7A87D7E507031BBCDF6C0DB095AFB21965 (void);
// 0x00000021 System.Void CharacterController2D::.ctor()
extern void CharacterController2D__ctor_m5C93DA812470EF6163E25545DED6CA554BBA83F7 (void);
// 0x00000022 System.Void CharacterController2D/BoolEvent::.ctor()
extern void BoolEvent__ctor_mB1C4B8CED2296E60F44636855900EA1BDF1F393A (void);
// 0x00000023 System.Void DamageIndicator::Start()
extern void DamageIndicator_Start_m4B11F0BBDA0E041B7CC3D21229D415AF5C8C4EF4 (void);
// 0x00000024 System.Void DamageIndicator::Update()
extern void DamageIndicator_Update_m464572748BC80B633E7824A9866A9E47BE399184 (void);
// 0x00000025 System.Void DamageIndicator::SetText(System.String)
extern void DamageIndicator_SetText_mF97716CDCCE4E2B072FB54D8EEE63ABB6AFD72B7 (void);
// 0x00000026 System.Void DamageIndicator::SetTextAndTimer(System.String)
extern void DamageIndicator_SetTextAndTimer_m78D55BA6F64B7BE16EAB69F3BF17D728A4D47906 (void);
// 0x00000027 System.Void DamageIndicator::.ctor()
extern void DamageIndicator__ctor_m829527469E0E1B01175A96EDB17FDEB95236C962 (void);
// 0x00000028 System.Void King::Start()
extern void King_Start_m69981586CA9F43BF63B90B6B3CE94DE7D5887F18 (void);
// 0x00000029 System.Void King::Update()
extern void King_Update_m5791314117CB9A57DEB4AE524E23B3B27E491BA6 (void);
// 0x0000002A System.Void King::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void King_OnTriggerEnter2D_mF632A4325E96D06F490F6EC99546591793E3D600 (void);
// 0x0000002B System.Void King::OnTriggerStay2D(UnityEngine.Collider2D)
extern void King_OnTriggerStay2D_mD29D1D718F19E886C850EBFCE6724E10D16673FA (void);
// 0x0000002C System.Void King::Attack()
extern void King_Attack_m563523658F2ABD9B8E0FAD48BE85DFF918308AD6 (void);
// 0x0000002D System.Void King::TakeDamage(System.Int32)
extern void King_TakeDamage_m29A678073F6207C6B7AF5F53A46A7250325EFC8E (void);
// 0x0000002E System.Void King::OnDeath()
extern void King_OnDeath_m4C854BC2DF9B102972DA734568C19ECF487E27A8 (void);
// 0x0000002F System.Void King::.ctor()
extern void King__ctor_m928AA3E86E3C2071EECB6DCD071F40251F6D0E54 (void);
// 0x00000030 System.Void Looter::Start()
extern void Looter_Start_m2318EA0F070BC32FD5463CCA485FE52EA5CEDCF1 (void);
// 0x00000031 System.Void Looter::Update()
extern void Looter_Update_mA497B96458912A20F5489F7FD33C3EFAEBEE2CD3 (void);
// 0x00000032 System.Void Looter::FixedUpdate()
extern void Looter_FixedUpdate_m1C3BC021A5129008AB0EF8FCF46CE56781C29220 (void);
// 0x00000033 System.Void Looter::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Looter_OnTriggerEnter2D_m23B9D20A99CE4FFACC27539A2D52896E250D85B8 (void);
// 0x00000034 System.Void Looter::OnTriggerStay2D(UnityEngine.Collider2D)
extern void Looter_OnTriggerStay2D_mAAFD2215D7B7C6ABEC53DDAEAF2CD4DAF88BA2A6 (void);
// 0x00000035 System.Void Looter::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Looter_OnTriggerExit2D_mC65EEBF9E2F01AF14477BCD539BE97B3B3C53986 (void);
// 0x00000036 System.Void Looter::GetNextPosition()
extern void Looter_GetNextPosition_mACFA8A5A1B0F1ABD5A9B6690ABDCA7B77745D2C9 (void);
// 0x00000037 System.Void Looter::TakeDamage(System.Int32)
extern void Looter_TakeDamage_mE68A87219643A040ECE336B4BE1C359ACF3ECB13 (void);
// 0x00000038 System.Void Looter::SetHealth(System.Int32)
extern void Looter_SetHealth_mB9657F33D11B013B2089CCD283EC2ED0057404BC (void);
// 0x00000039 System.Void Looter::OnDeath()
extern void Looter_OnDeath_m452AF4C76398A61937EB7EC3C234C3AF25D98F64 (void);
// 0x0000003A System.Void Looter::.ctor()
extern void Looter__ctor_m6E133B7DA662A570549B173EFF93C938D8DFB0BA (void);
// 0x0000003B System.Void MainMenu::PlayGame()
extern void MainMenu_PlayGame_m96A3CE2743BCB00B665AA3AC575AE4EBD9ED40B0 (void);
// 0x0000003C System.Void MainMenu::QuitGame()
extern void MainMenu_QuitGame_m9F32E266C6F6CE345067D062258362159D267030 (void);
// 0x0000003D System.Void MainMenu::.ctor()
extern void MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D (void);
// 0x0000003E System.Void Player::Start()
extern void Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021 (void);
// 0x0000003F System.Void Player::Update()
extern void Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3 (void);
// 0x00000040 System.Void Player::FixedUpdate()
extern void Player_FixedUpdate_mD7447EDFC86F29A3E5FBDEF7E0139535BD4C5088 (void);
// 0x00000041 System.Void Player::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Player_OnCollisionEnter2D_m0448BB6F02AAB4967D3AB976DA22A24ACD8E7F90 (void);
// 0x00000042 System.Void Player::OnCollisionStay2D(UnityEngine.Collision2D)
extern void Player_OnCollisionStay2D_m81D8A16E9CB60D15F075E7A339EF5FF98475D680 (void);
// 0x00000043 System.Void Player::TakeDamage(System.Int32,System.Boolean)
extern void Player_TakeDamage_m11E35C8E11AB669B270A7498623D779590182DFF (void);
// 0x00000044 System.Void Player::Attack()
extern void Player_Attack_mDBF5675D49B5FEAF3B69717FA732B2FCAC54AB98 (void);
// 0x00000045 System.Int32 Player::GetDealtDamage()
extern void Player_GetDealtDamage_mE510DA7BBF42112779340233BC1DF6475B0E59D0 (void);
// 0x00000046 System.Void Player::Stop()
extern void Player_Stop_m1C3A5D2B110766366C98792A0E80BCE220554B3E (void);
// 0x00000047 System.Void Player::QuitApp()
extern void Player_QuitApp_m89E1ABAE0B609D6B2FE68795D2DC97FD9234E1E5 (void);
// 0x00000048 System.Void Player::.ctor()
extern void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (void);
// 0x00000049 System.Void Princess::Start()
extern void Princess_Start_m53049A703127B9DC0045AFA68F0B4172699759A0 (void);
// 0x0000004A System.Void Princess::Update()
extern void Princess_Update_mC4B9B858694510B7BE89078C12B980CF9CB8A0CF (void);
// 0x0000004B System.Void Princess::FixedUpdate()
extern void Princess_FixedUpdate_mF45DE393E3B4D5BA8C3BB3D30909F1F91173EAA1 (void);
// 0x0000004C System.Void Princess::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Princess_OnTriggerEnter2D_m238228C39DE13426DBD3D2D2485CBB2B8EE226E3 (void);
// 0x0000004D System.Void Princess::OnTriggerStay2D(UnityEngine.Collider2D)
extern void Princess_OnTriggerStay2D_m51F88039C8B9770EC38F296D0914161E77984EA5 (void);
// 0x0000004E System.Void Princess::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Princess_OnTriggerExit2D_m7D64A1E1F6B0727C0320FE0B81E003C09C66027F (void);
// 0x0000004F System.Void Princess::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Princess_OnCollisionEnter2D_m01C2B0AE0F75373BBDFBD98FCCAEBC78BDBB96B3 (void);
// 0x00000050 System.Void Princess::TakeDamage(System.Int32)
extern void Princess_TakeDamage_mDA45461DC925A552C072DA18031F00B1CA0E0838 (void);
// 0x00000051 System.Void Princess::SetHealth(System.Int32)
extern void Princess_SetHealth_mFDFF208312C2D18A5A7851EA01770EE06A6CFA62 (void);
// 0x00000052 System.Void Princess::OnDeath()
extern void Princess_OnDeath_mC7EF0DC5318CADA6AF3D7D5D69105F184496269C (void);
// 0x00000053 System.Void Princess::Panic()
extern void Princess_Panic_mE25CDB1E442F2C1A479C0061CAB65B2236A9895A (void);
// 0x00000054 System.Void Princess::.ctor()
extern void Princess__ctor_m36185E3125E99DE33380F8D4ED4D58BB7C84E20D (void);
// 0x00000055 System.Void Runner::Start()
extern void Runner_Start_mD09A99E8C2108694F858656063D1E3F62B5C2373 (void);
// 0x00000056 System.Void Runner::Update()
extern void Runner_Update_m52DE8D69CFDBFBA32D8E861563463E64C6247B19 (void);
// 0x00000057 System.Void Runner::FixedUpdate()
extern void Runner_FixedUpdate_m5ACC762150AC6CCD90809EE5786EB08E175D6F42 (void);
// 0x00000058 System.Void Runner::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Runner_OnTriggerEnter2D_mF3A121ABFE6BC3A6CDCB5405A628F07694900E20 (void);
// 0x00000059 System.Void Runner::OnTriggerStay2D(UnityEngine.Collider2D)
extern void Runner_OnTriggerStay2D_mEEDB68CFBE21FBBF0D22FDEE3B10CC09824000BA (void);
// 0x0000005A System.Void Runner::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Runner_OnTriggerExit2D_mE8EBB886C33465195CBC2A57D62D1A39C045EC5A (void);
// 0x0000005B System.Void Runner::TakeDamage(System.Int32)
extern void Runner_TakeDamage_m9BBCEDD903FCCDA5C746F822C9B00D6B7BE89B1A (void);
// 0x0000005C System.Void Runner::SetHealth(System.Int32)
extern void Runner_SetHealth_mC817B3E4C747FD01FA8A0ECFCFCC4562CFE58851 (void);
// 0x0000005D System.Void Runner::OnDeath()
extern void Runner_OnDeath_m9314366CAA8AAC09F9AEBE40D21583DACDF53564 (void);
// 0x0000005E System.Void Runner::Panic()
extern void Runner_Panic_m03F977FB97B10BA32CACF139A86A4A43911CB49A (void);
// 0x0000005F System.Void Runner::.ctor()
extern void Runner__ctor_m90AC232C165056FBD851D53F1689BD5FC15563FA (void);
// 0x00000060 System.Void Shooter::Start()
extern void Shooter_Start_mBA83984BC9920FA1D7FF4B5F60389CC102F8CE77 (void);
// 0x00000061 System.Void Shooter::Update()
extern void Shooter_Update_mF95D0E122742F65FF3236CC03DDE9CA17DDCB7E2 (void);
// 0x00000062 System.Void Shooter::FixedUpdate()
extern void Shooter_FixedUpdate_mAE5EF3A229F96A18D817674F378479A86BA0CB1F (void);
// 0x00000063 System.Void Shooter::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Shooter_OnTriggerEnter2D_mACB5436967FD849BB98149421C5C2600B5CC92F4 (void);
// 0x00000064 System.Void Shooter::OnTriggerStay2D(UnityEngine.Collider2D)
extern void Shooter_OnTriggerStay2D_mB9D2D282C2FBAD9213499D47A3B7E735DF4EAF1D (void);
// 0x00000065 System.Void Shooter::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Shooter_OnTriggerExit2D_mF40AC4779682CFCD3A6D2B91F8180B9B1A42197C (void);
// 0x00000066 System.Boolean Shooter::get_ShootToKill()
extern void Shooter_get_ShootToKill_m8FF035DCEB12C201A7F91678A26464F55440CD67 (void);
// 0x00000067 System.Void Shooter::set_ShootToKill(System.Boolean)
extern void Shooter_set_ShootToKill_m5A910F98A663E778F8C3E14AD6A18C34CFD0A3FB (void);
// 0x00000068 System.Single Shooter::GetShootDirection()
extern void Shooter_GetShootDirection_m863DA4A194D3695648318F8E0274848F7787B7B8 (void);
// 0x00000069 System.Void Shooter::SetShootDirection(System.Single)
extern void Shooter_SetShootDirection_m57E3C5933CF47E219CEE623AC6D994C8EFE615B5 (void);
// 0x0000006A System.Single Shooter::get_ShootRate()
extern void Shooter_get_ShootRate_m13A64EFA275A9AA5432951838302AA1BC668D6DA (void);
// 0x0000006B System.Void Shooter::set_ShootRate(System.Single)
extern void Shooter_set_ShootRate_mC1750D830B325B9E82D7EE73C294A66AD80EFC1B (void);
// 0x0000006C System.Single Shooter::get_ShootSpeed()
extern void Shooter_get_ShootSpeed_m68331A226E5CD07EF49F80E6BABACD7566632BC9 (void);
// 0x0000006D System.Void Shooter::set_ShootSpeed(System.Single)
extern void Shooter_set_ShootSpeed_m33024A121B7BCBC28302569586D0DA41EA94F403 (void);
// 0x0000006E System.Void Shooter::TakeDamage(System.Int32)
extern void Shooter_TakeDamage_m61D297CBAC0ADC70E7F37CB261A157D43E663285 (void);
// 0x0000006F System.Void Shooter::SetHealth(System.Int32)
extern void Shooter_SetHealth_m1FA2DC71118E52EA760BEB115DC02EC889B37ABD (void);
// 0x00000070 System.Void Shooter::OnDeath()
extern void Shooter_OnDeath_mA53E550258EA3475F42F809088DE6BE100BE33F5 (void);
// 0x00000071 System.Void Shooter::.ctor()
extern void Shooter__ctor_m173C774BF5668018A0B10ACA4BB5A6DA3A6BAF2D (void);
// 0x00000072 System.Void Spawner::Start()
extern void Spawner_Start_m61D97BD980BD1B1877634A1E7626E47418D5D6D8 (void);
// 0x00000073 System.Void Spawner::Spawn()
extern void Spawner_Spawn_m9A3438B8559369C0BC1578C2AE341CCA181E6B6C (void);
// 0x00000074 System.Single Spawner::GetSpawnRadius()
extern void Spawner_GetSpawnRadius_m0B0717C35412E6DEDE11B995E35F734D500834EC (void);
// 0x00000075 System.Single Spawner::GetTotal()
extern void Spawner_GetTotal_mD93FFF5C48556D87C319267B0AAEA9C13E1929E6 (void);
// 0x00000076 System.Void Spawner::.ctor()
extern void Spawner__ctor_m08E8D40AAA40F4329D8A95EEE2B2B6BE842CEB9C (void);
// 0x00000077 System.Void Spike::Start()
extern void Spike_Start_m60979F603D0D0A5A73706AD7FF71B8BA14FE64A3 (void);
// 0x00000078 System.Void Spike::Update()
extern void Spike_Update_m130B8AC280A3390D6F67CEFC28C4C7E7B0ADFA49 (void);
// 0x00000079 System.Void Spike::.ctor()
extern void Spike__ctor_m5908FB939F6D9A6FBFE73C02127CF54079DA8BB1 (void);
// 0x0000007A System.Void Trigger::Start()
extern void Trigger_Start_m3A9539F692683C0656862AF715A3155BE7235812 (void);
// 0x0000007B System.Void Trigger::OnTriggerExit(UnityEngine.Collider)
extern void Trigger_OnTriggerExit_mD9036B187B8A47B94E9F03CF4287F0DFFF99C8CF (void);
// 0x0000007C System.Void Trigger::OnTriggerEnter(UnityEngine.Collider)
extern void Trigger_OnTriggerEnter_mBC7228041960CB9DB4ED33DBA9513A8DAFDC862A (void);
// 0x0000007D System.Void Trigger::.ctor()
extern void Trigger__ctor_m42036DF1067B2973F4CF5F07DF83F4162A21081C (void);
// 0x0000007E System.Void UI_Text::Start()
extern void UI_Text_Start_m7FD933C09A4FB9ECCC5D261D1AED22E344101C48 (void);
// 0x0000007F System.Void UI_Text::Update()
extern void UI_Text_Update_m05F21BDD61C4B88D88C5AAB124772A6CE47F9D0E (void);
// 0x00000080 System.Void UI_Text::SetText(System.String)
extern void UI_Text_SetText_m21691663AC1558A788D56FC9C47F263D250A0235 (void);
// 0x00000081 System.Void UI_Text::SetColor(UnityEngine.Color)
extern void UI_Text_SetColor_m7E211B828CA484914CEBE7F108E7447410561B1E (void);
// 0x00000082 System.Void UI_Text::SetTextAndTimer(System.String)
extern void UI_Text_SetTextAndTimer_m8EEF10F93362E077EE68862D75C2255533698011 (void);
// 0x00000083 System.Void UI_Text::.ctor()
extern void UI_Text__ctor_m5932DBE70BF136736B8B9A21A1407381B3B020D5 (void);
// 0x00000084 System.Void Demo::Start()
extern void Demo_Start_mA96D71BE495F276B026F0223EAAF21A1B4C626E8 (void);
// 0x00000085 System.Void Demo::Click()
extern void Demo_Click_m3027181571AD1AC23FE4E3C7A0C03F5E21983966 (void);
// 0x00000086 System.Void Demo::TogglePause()
extern void Demo_TogglePause_m1B9D3936E6A3A4B8FA372E0C5184FF9D32C2A7C0 (void);
// 0x00000087 System.Void Demo::OnEnable()
extern void Demo_OnEnable_m18912F888C952F5A063CE0A6A3A9F57C3A05314C (void);
// 0x00000088 System.Void Demo::OnDisable()
extern void Demo_OnDisable_mF53457E7F2E00A5AC6FBA9A5526150DB5550DDBA (void);
// 0x00000089 System.Void Demo::.ctor()
extern void Demo__ctor_mA1BCCF87BB86D1772EABA9E89B452959AA579645 (void);
// 0x0000008A System.Single SMSound::GetVolume()
extern void SMSound_GetVolume_mC43AED7BE161373FD58012DB293D81EAD770989A (void);
// 0x0000008B SMSound SMSound::SetVolume(System.Single)
extern void SMSound_SetVolume_m58AA414EDCBCB36BA262F80055F0153CFEC86F5B (void);
// 0x0000008C SMSound SMSound::SetLooped(System.Boolean)
extern void SMSound_SetLooped_m913D374DA0D8764D112FBDF7BA4450F5E42F756B (void);
// 0x0000008D SMSound SMSound::SetPausable(System.Boolean)
extern void SMSound_SetPausable_m43D52A214E7B163226BDCCEA64E428CAAEFE34F3 (void);
// 0x0000008E SMSound SMSound::Set3D(System.Boolean)
extern void SMSound_Set3D_m3807F763F5091B98C6525BDCDAF9A22CCAA0C836 (void);
// 0x0000008F SMSound SMSound::AttachToObject(UnityEngine.Transform)
extern void SMSound_AttachToObject_m7B3BD9B16D547C25CDD544FB4F483542A926CBF6 (void);
// 0x00000090 SMSound SMSound::SetPosition(UnityEngine.Vector3)
extern void SMSound_SetPosition_m04B12FF50E47B12AF7458778206DE27AE9280A8D (void);
// 0x00000091 System.Void SMSound::Stop()
extern void SMSound_Stop_m4CDC150347B24264A8436F331CB139CBCF1C4BDC (void);
// 0x00000092 System.Void SMSound::.ctor()
extern void SMSound__ctor_m4C279C963F40290FECD10E090D673B0EB81ABAC6 (void);
// 0x00000093 System.Void SoundManager::PlayMusic(System.String)
extern void SoundManager_PlayMusic_m6251F4C75B16CFD4581C0C63F3CDAC4C2E0D0694 (void);
// 0x00000094 System.Void SoundManager::StopMusic()
extern void SoundManager_StopMusic_mECA9EABD5243BD1D37D962FBBCA619055563411A (void);
// 0x00000095 SMSound SoundManager::PlaySound(UnityEngine.AudioClip)
extern void SoundManager_PlaySound_m587A71005C68154DD9ED8003394F77B87266EB8E (void);
// 0x00000096 SMSound SoundManager::PlaySoundUI(UnityEngine.AudioClip)
extern void SoundManager_PlaySoundUI_mB8E07596B5F136248D0D3FBD5361E8AFA71EA3A9 (void);
// 0x00000097 SMSound SoundManager::PlaySound(System.String,UnityEngine.AssetBundle)
extern void SoundManager_PlaySound_m0C148519D3AED0D81A139EF621A4C5CFAF10AEE5 (void);
// 0x00000098 SMSound SoundManager::PlaySoundUI(System.String,UnityEngine.AssetBundle)
extern void SoundManager_PlaySoundUI_mC929F5103AF02FBD9F3FD10EA93F6F6F8A18297E (void);
// 0x00000099 SMSound SoundManager::PlaySound(System.String)
extern void SoundManager_PlaySound_m67F215B6697BE0670B85D53785476B8989799CBA (void);
// 0x0000009A SMSound SoundManager::PlaySoundUI(System.String)
extern void SoundManager_PlaySoundUI_mED1AA4F61D0C832C1750733C5B6B0357E4530B4F (void);
// 0x0000009B System.Void SoundManager::PlaySoundWithDelay(System.String,System.Single,System.Boolean)
extern void SoundManager_PlaySoundWithDelay_m07CC473A3F7F4D29225B16E5465E1E3947FF0590 (void);
// 0x0000009C System.Void SoundManager::LoadSound(System.String)
extern void SoundManager_LoadSound_m56A3465AA637ACC641C819B00224CC3903D5D1BA (void);
// 0x0000009D System.Void SoundManager::UnloadSound(System.String,System.Boolean)
extern void SoundManager_UnloadSound_mAA549E53194BD1A0A610EEB36998F5FB4A1BF5EA (void);
// 0x0000009E System.Void SoundManager::Pause()
extern void SoundManager_Pause_mCE7BD62E35460BB4C8B4C0289BA55F34ADD6D4DC (void);
// 0x0000009F System.Void SoundManager::UnPause()
extern void SoundManager_UnPause_m478BB470970DDC471BFB25D98D5D4EE934224823 (void);
// 0x000000A0 System.Void SoundManager::StopAllPausableSounds()
extern void SoundManager_StopAllPausableSounds_mA4C1EED3EA9463FF48803FCD2B07A87390A4C4C6 (void);
// 0x000000A1 System.Void SoundManager::SetMusicVolume(System.Single)
extern void SoundManager_SetMusicVolume_m3DCEA13F93A1885620C5A0239B1F225D52C6EDC0 (void);
// 0x000000A2 System.Single SoundManager::GetMusicVolume()
extern void SoundManager_GetMusicVolume_m8A00ACEB320E2DC02BB1012794432F5BCC800D76 (void);
// 0x000000A3 System.Void SoundManager::SetMusicMuted(System.Boolean)
extern void SoundManager_SetMusicMuted_m6AEE27FAD4AD5F2D8B0F8AEE61A0B1585BD2BDBF (void);
// 0x000000A4 System.Boolean SoundManager::GetMusicMuted()
extern void SoundManager_GetMusicMuted_mE0084F382B3DAFF61FC98F3DA3665C2E81F61337 (void);
// 0x000000A5 System.Void SoundManager::SetSoundVolume(System.Single)
extern void SoundManager_SetSoundVolume_m6600DFCCAF9F424D4D5C5AB545B9D7CCD703B48C (void);
// 0x000000A6 System.Single SoundManager::GetSoundVolume()
extern void SoundManager_GetSoundVolume_m3AE9165E1F72BB65442B1F4E4E09A91C32198DE4 (void);
// 0x000000A7 System.Void SoundManager::SetSoundMuted(System.Boolean)
extern void SoundManager_SetSoundMuted_mCF38F4FC1705C73B33A0E95FB76A358312644ED0 (void);
// 0x000000A8 System.Boolean SoundManager::GetSoundMuted()
extern void SoundManager_GetSoundMuted_mE08B2FC4C79D52ED2A878EBD6386BD658E08BED1 (void);
// 0x000000A9 System.Boolean SoundManager::IsValid()
extern void SoundManager_IsValid_m6FB686E39EC39949F1BE858F38294D93117D1FBF (void);
// 0x000000AA SoundManagerSettings SoundManager::GetSettings()
extern void SoundManager_GetSettings_m877365E28CAA00980917E0E52E41E54934AA431A (void);
// 0x000000AB System.Void SoundManager::Stop(SMSound)
extern void SoundManager_Stop_m782738B5E79C91EF3E34D56C5428AE1D2DF6F8F7 (void);
// 0x000000AC SoundManager SoundManager::get_Instance()
extern void SoundManager_get_Instance_mB2D05669AF91B4D14A63BC76820A362B6F36AB90 (void);
// 0x000000AD System.Void SoundManager::OnDestroy()
extern void SoundManager_OnDestroy_m251DAD37E7BEDCEA021D39A9E132CF610BF43FBF (void);
// 0x000000AE System.Void SoundManager::PlayMusicInternal(System.String)
extern void SoundManager_PlayMusicInternal_mE9BA4EC365B0FEAFB096F4443E19C9BD76DF68BE (void);
// 0x000000AF System.Void SoundManager::StopMusicInternal()
extern void SoundManager_StopMusicInternal_mA18857574BB02C6AF1BD8994CD3FC880D0CF0A58 (void);
// 0x000000B0 SMSound SoundManager::PlaySoundInternal(System.String,System.Boolean,UnityEngine.AssetBundle)
extern void SoundManager_PlaySoundInternal_mAA60F2DB6288927FCDE27CA874DDB6C68676A65D (void);
// 0x000000B1 SMSound SoundManager::PlaySoundClipInternal(UnityEngine.AudioClip,System.Boolean)
extern void SoundManager_PlaySoundClipInternal_m85A8840CB6CC5264B9BA9B08B118609904F17F8F (void);
// 0x000000B2 System.Collections.IEnumerator SoundManager::PlaySoundInternalAfterLoad(SMSound,System.String,UnityEngine.AssetBundle)
extern void SoundManager_PlaySoundInternalAfterLoad_m69CE512AB33E3672FC4DB3E26529D4AD2E9A1B88 (void);
// 0x000000B3 System.Void SoundManager::PlaySoundWithDelayInternal(System.String,System.Single,System.Boolean)
extern void SoundManager_PlaySoundWithDelayInternal_m07DD8B3B99F9255D3AB8F73EE27374209B1D7A04 (void);
// 0x000000B4 System.Void SoundManager::StopAllPausableSoundsInternal()
extern void SoundManager_StopAllPausableSoundsInternal_mAAD00174BF7D34434314E32CAFE22B2E0BB137BD (void);
// 0x000000B5 System.Void SoundManager::StopSoundInternal(SMSound)
extern void SoundManager_StopSoundInternal_mF12FAB38DDC1F46F9CEAE028357CE35AD9C67706 (void);
// 0x000000B6 System.Void SoundManager::LoadSoundInternal(System.String)
extern void SoundManager_LoadSoundInternal_mE69272F4A3F06CC62F914050EC859F5AE3AE70D5 (void);
// 0x000000B7 System.Void SoundManager::UnloadSoundInternal(System.String,System.Boolean)
extern void SoundManager_UnloadSoundInternal_m968B8AA16ECD0E4B18EE4AAD59D6F8AD349DF7E8 (void);
// 0x000000B8 System.Void SoundManager::Awake()
extern void SoundManager_Awake_m78F953F39CFB3F539240E1226D04270B793B1A76 (void);
// 0x000000B9 System.Void SoundManager::Update()
extern void SoundManager_Update_mDD188B65FF1E9B1DF1B8345A6290D149E70E657C (void);
// 0x000000BA System.Void SoundManager::LateUpdate()
extern void SoundManager_LateUpdate_m76E0C0FA7F82739334A4F04E457F47B08C971719 (void);
// 0x000000BB System.Void SoundManager::StartFadingOutMusic()
extern void SoundManager_StartFadingOutMusic_m2296A245F1A0255B82AAF14FC7AA41BCBA9DA17B (void);
// 0x000000BC System.Collections.IEnumerator SoundManager::PlaySoundWithDelayCoroutine(System.String,System.Single,System.Boolean)
extern void SoundManager_PlaySoundWithDelayCoroutine_m0110575D6B5522AE00F1613CC6EA6D75E12233DF (void);
// 0x000000BD UnityEngine.AudioClip SoundManager::LoadClip(System.String)
extern void SoundManager_LoadClip_m99E0190157845EC162ADDFAD1F0EFBDDC9B3FF89 (void);
// 0x000000BE UnityEngine.ResourceRequest SoundManager::LoadClipAsync(System.String)
extern void SoundManager_LoadClipAsync_mCCFE6DF970CA8D3B19626B2C1F08F5E44AC630EB (void);
// 0x000000BF UnityEngine.AssetBundleRequest SoundManager::LoadClipFromBundleAsync(UnityEngine.AssetBundle,System.String)
extern void SoundManager_LoadClipFromBundleAsync_m7DB5E73164ED90CB63E7BA0128F2DF341F0C6919 (void);
// 0x000000C0 System.Boolean SoundManager::IsSoundFinished(SMSound)
extern void SoundManager_IsSoundFinished_mFAA2F4D8C7E0C6E2BFA4AC49B7735EE28A4FE3F1 (void);
// 0x000000C1 System.Void SoundManager::ApplySoundVolume()
extern void SoundManager_ApplySoundVolume_m7CAB25628C9F40061943E554BA349057A3BF501F (void);
// 0x000000C2 System.Void SoundManager::ApplyMusicVolume()
extern void SoundManager_ApplyMusicVolume_mECB66840BEAFC21585E4D79C55264D93912C9C65 (void);
// 0x000000C3 System.Void SoundManager::ApplySoundMuted()
extern void SoundManager_ApplySoundMuted_mA17F5B0880B023141ED24C0D79BC6EED7920E133 (void);
// 0x000000C4 System.Void SoundManager::ApplyMusicMuted()
extern void SoundManager_ApplyMusicMuted_m45D3B059FF30F80FC1EB0CD63200205FD5FA8392 (void);
// 0x000000C5 System.Void SoundManager::.ctor()
extern void SoundManager__ctor_m30D18BC25871BD3FF7FB195A1CAE50A2EE48FCAE (void);
// 0x000000C6 System.Void SoundManager::.cctor()
extern void SoundManager__cctor_mCB4F772852263D66AF3536FB81B715BD441B6A2C (void);
// 0x000000C7 System.Void SoundManager/SMMusic::.ctor()
extern void SMMusic__ctor_m5EDCBB5468C954967E0F6F0C614585120C60E7C5 (void);
// 0x000000C8 System.Void SoundManager/SMMusicFadingOut::.ctor()
extern void SMMusicFadingOut__ctor_m8FE1B268D59CE43E83B52E0CEF90E32DD734929E (void);
// 0x000000C9 System.Void SoundManager/<PlaySoundInternalAfterLoad>d__45::.ctor(System.Int32)
extern void U3CPlaySoundInternalAfterLoadU3Ed__45__ctor_mD6064B0F2DDD2C4935997AF85C3A1930245D20E7 (void);
// 0x000000CA System.Void SoundManager/<PlaySoundInternalAfterLoad>d__45::System.IDisposable.Dispose()
extern void U3CPlaySoundInternalAfterLoadU3Ed__45_System_IDisposable_Dispose_mD9A491C7F3B9F7B5075D2C2948D7BE2F94B689A5 (void);
// 0x000000CB System.Boolean SoundManager/<PlaySoundInternalAfterLoad>d__45::MoveNext()
extern void U3CPlaySoundInternalAfterLoadU3Ed__45_MoveNext_mEE0760C84CDD8948D7FB13FA7033AAA83185F8A1 (void);
// 0x000000CC System.Object SoundManager/<PlaySoundInternalAfterLoad>d__45::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlaySoundInternalAfterLoadU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE414256812BA6540771F8F9B5EA2721869F2D183 (void);
// 0x000000CD System.Void SoundManager/<PlaySoundInternalAfterLoad>d__45::System.Collections.IEnumerator.Reset()
extern void U3CPlaySoundInternalAfterLoadU3Ed__45_System_Collections_IEnumerator_Reset_m30A5FDEC90A34FE45B02AAF2DA653A294C2C7698 (void);
// 0x000000CE System.Object SoundManager/<PlaySoundInternalAfterLoad>d__45::System.Collections.IEnumerator.get_Current()
extern void U3CPlaySoundInternalAfterLoadU3Ed__45_System_Collections_IEnumerator_get_Current_m8C7F70D3636F110215CA91C2B44EB6C503CAA24A (void);
// 0x000000CF System.Void SoundManager/<PlaySoundWithDelayCoroutine>d__55::.ctor(System.Int32)
extern void U3CPlaySoundWithDelayCoroutineU3Ed__55__ctor_mCB5E5B2BE111444B588F31BA0C5AFD7509B69712 (void);
// 0x000000D0 System.Void SoundManager/<PlaySoundWithDelayCoroutine>d__55::System.IDisposable.Dispose()
extern void U3CPlaySoundWithDelayCoroutineU3Ed__55_System_IDisposable_Dispose_m7817CCF7616CF50FF520EE581D0CA567D3380903 (void);
// 0x000000D1 System.Boolean SoundManager/<PlaySoundWithDelayCoroutine>d__55::MoveNext()
extern void U3CPlaySoundWithDelayCoroutineU3Ed__55_MoveNext_m40B6DF85C7E6F41AB7ECCA7CC2AF223EBCF24597 (void);
// 0x000000D2 System.Object SoundManager/<PlaySoundWithDelayCoroutine>d__55::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlaySoundWithDelayCoroutineU3Ed__55_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC01FA9B1E453E98F34D9ACC6366AEAA36AAAC5B4 (void);
// 0x000000D3 System.Void SoundManager/<PlaySoundWithDelayCoroutine>d__55::System.Collections.IEnumerator.Reset()
extern void U3CPlaySoundWithDelayCoroutineU3Ed__55_System_Collections_IEnumerator_Reset_m51984042EB5E01171299361F7E1DCFF91D5E5C35 (void);
// 0x000000D4 System.Object SoundManager/<PlaySoundWithDelayCoroutine>d__55::System.Collections.IEnumerator.get_Current()
extern void U3CPlaySoundWithDelayCoroutineU3Ed__55_System_Collections_IEnumerator_get_Current_mD113099C5BD2CEF9A9D657CD45880CB128609A23 (void);
// 0x000000D5 System.Void SoundManagerComponent::PlaySound(System.String)
extern void SoundManagerComponent_PlaySound_mFBA43D406AB3B7E122C732BD2488746076D521C0 (void);
// 0x000000D6 System.Void SoundManagerComponent::PlaySoundNotPausable(System.String)
extern void SoundManagerComponent_PlaySoundNotPausable_mAAF03ACECC1FADC86DCE3FF6540541D8CE5F4A71 (void);
// 0x000000D7 System.Void SoundManagerComponent::ChangeSoundVolume(System.Single)
extern void SoundManagerComponent_ChangeSoundVolume_m9C2A069197722F8323FF30AF7FEDFFD7AB9A792B (void);
// 0x000000D8 System.Void SoundManagerComponent::ChangeMusicVolume(System.Single)
extern void SoundManagerComponent_ChangeMusicVolume_mCAF30EDB0D5A6CF85043FE191B228796FFF6E797 (void);
// 0x000000D9 System.Void SoundManagerComponent::ToggleMusicMuted()
extern void SoundManagerComponent_ToggleMusicMuted_m3FB62421AC201F9DCFA9A5912B71612BE74CE7FB (void);
// 0x000000DA System.Void SoundManagerComponent::ToggleSoundMuted()
extern void SoundManagerComponent_ToggleSoundMuted_m6F317B59789580E114851CEAB638114D74552B2D (void);
// 0x000000DB System.Void SoundManagerComponent::.ctor()
extern void SoundManagerComponent__ctor_m1EFC625BD85A6AC60B68532BB3E3B3A8339865C8 (void);
// 0x000000DC System.Void SoundManagerSettings::SaveSettings()
extern void SoundManagerSettings_SaveSettings_m0E8B6E03EBA5A0520B468788211CCCF3E82CCD33 (void);
// 0x000000DD System.Void SoundManagerSettings::LoadSettings()
extern void SoundManagerSettings_LoadSettings_m171B6F4C319015EB3FE75F18BA130B23CC8EC594 (void);
// 0x000000DE System.Void SoundManagerSettings::SetMusicVolume(System.Single)
extern void SoundManagerSettings_SetMusicVolume_m0F4DF6F9A63C77149C24B57AA241ACD5EE115698 (void);
// 0x000000DF System.Single SoundManagerSettings::GetMusicVolume()
extern void SoundManagerSettings_GetMusicVolume_m20193DF9974CEE67FE71CD1165DBAB2076A0E8F9 (void);
// 0x000000E0 System.Void SoundManagerSettings::SetSoundVolume(System.Single)
extern void SoundManagerSettings_SetSoundVolume_mD30B9C446781EEF76D2FF2066192E7AF58FAF390 (void);
// 0x000000E1 System.Single SoundManagerSettings::GetSoundVolume()
extern void SoundManagerSettings_GetSoundVolume_mAC515D97E233056D18F56065B7B020EA05CBC8BC (void);
// 0x000000E2 System.Void SoundManagerSettings::SetMusicMuted(System.Boolean)
extern void SoundManagerSettings_SetMusicMuted_m8A98932A4C78C4DF6F508E656F4099727A1763F7 (void);
// 0x000000E3 System.Boolean SoundManagerSettings::GetMusicMuted()
extern void SoundManagerSettings_GetMusicMuted_m4FBFB0E680A90C4498C4C53864DA0B16445FDB5A (void);
// 0x000000E4 System.Void SoundManagerSettings::SetSoundMuted(System.Boolean)
extern void SoundManagerSettings_SetSoundMuted_m50610F4FDCA933C111F797ECB692CE5CCA05B88E (void);
// 0x000000E5 System.Boolean SoundManagerSettings::GetSoundMuted()
extern void SoundManagerSettings_GetSoundMuted_m5EB7F422BCEBDED4A082DF639F084194169A60EE (void);
// 0x000000E6 System.Single SoundManagerSettings::GetSoundVolumeCorrected()
extern void SoundManagerSettings_GetSoundVolumeCorrected_m92A031C60B6497D6AEF654419ABA27693CEA3C2F (void);
// 0x000000E7 System.Single SoundManagerSettings::GetMusicVolumeCorrected()
extern void SoundManagerSettings_GetMusicVolumeCorrected_m8DAD6A1342EED1DB3CF50F0BDA3788E0E29E6F90 (void);
// 0x000000E8 System.Void SoundManagerSettings::.ctor()
extern void SoundManagerSettings__ctor_m65F81B28B6C9D067D362C71F1691085F078D00B4 (void);
// 0x000000E9 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2 (void);
// 0x000000EA System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B (void);
// 0x000000EB System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371 (void);
// 0x000000EC System.Void ChatController::.ctor()
extern void ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026 (void);
// 0x000000ED System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6 (void);
// 0x000000EE System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9 (void);
// 0x000000EF System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93 (void);
// 0x000000F0 System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE (void);
// 0x000000F1 System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC (void);
// 0x000000F2 System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23 (void);
// 0x000000F3 System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B (void);
// 0x000000F4 System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE (void);
// 0x000000F5 System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E (void);
// 0x000000F6 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB (void);
// 0x000000F7 System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF (void);
// 0x000000F8 System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226 (void);
// 0x000000F9 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E (void);
// 0x000000FA System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1 (void);
// 0x000000FB System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3 (void);
// 0x000000FC TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385 (void);
// 0x000000FD System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4 (void);
// 0x000000FE TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B (void);
// 0x000000FF System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788 (void);
// 0x00000100 TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A (void);
// 0x00000101 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076 (void);
// 0x00000102 TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D (void);
// 0x00000103 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA (void);
// 0x00000104 TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB (void);
// 0x00000105 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5 (void);
// 0x00000106 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70 (void);
// 0x00000107 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11 (void);
// 0x00000108 System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC (void);
// 0x00000109 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44 (void);
// 0x0000010A System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC (void);
// 0x0000010B System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53 (void);
// 0x0000010C System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66 (void);
// 0x0000010D System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43 (void);
// 0x0000010E System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77 (void);
// 0x0000010F System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8 (void);
// 0x00000110 System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7 (void);
// 0x00000111 System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2 (void);
// 0x00000112 System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C (void);
// 0x00000113 System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018 (void);
// 0x00000114 System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0 (void);
// 0x00000115 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C (void);
// 0x00000116 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F (void);
// 0x00000117 System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB (void);
// 0x00000118 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73 (void);
// 0x00000119 System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C (void);
// 0x0000011A System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591 (void);
// 0x0000011B System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B (void);
// 0x0000011C System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E (void);
// 0x0000011D System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4 (void);
// 0x0000011E System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376 (void);
// 0x0000011F System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651 (void);
// 0x00000120 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561 (void);
// 0x00000121 System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1 (void);
// 0x00000122 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD (void);
// 0x00000123 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA (void);
// 0x00000124 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2 (void);
// 0x00000125 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046 (void);
// 0x00000126 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5 (void);
// 0x00000127 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77 (void);
// 0x00000128 System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516 (void);
// 0x00000129 System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE (void);
// 0x0000012A System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9 (void);
// 0x0000012B System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C (void);
// 0x0000012C System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E (void);
// 0x0000012D System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610 (void);
// 0x0000012E System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373 (void);
// 0x0000012F System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3 (void);
// 0x00000130 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124 (void);
// 0x00000131 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA (void);
// 0x00000132 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3 (void);
// 0x00000133 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC (void);
// 0x00000134 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8 (void);
// 0x00000135 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2 (void);
// 0x00000136 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469 (void);
// 0x00000137 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64 (void);
// 0x00000138 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A (void);
// 0x00000139 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B (void);
// 0x0000013A System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF (void);
// 0x0000013B System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390 (void);
// 0x0000013C System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B (void);
// 0x0000013D System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362 (void);
// 0x0000013E System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F (void);
// 0x0000013F System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5 (void);
// 0x00000140 System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697 (void);
// 0x00000141 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB (void);
// 0x00000142 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F (void);
// 0x00000143 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3 (void);
// 0x00000144 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3 (void);
// 0x00000145 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86 (void);
// 0x00000146 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F (void);
// 0x00000147 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF (void);
// 0x00000148 System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF (void);
// 0x00000149 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1 (void);
// 0x0000014A System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F (void);
// 0x0000014B System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A (void);
// 0x0000014C System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4 (void);
// 0x0000014D System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB (void);
// 0x0000014E System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12 (void);
// 0x0000014F System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3 (void);
// 0x00000150 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720 (void);
// 0x00000151 System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21 (void);
// 0x00000152 System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1 (void);
// 0x00000153 System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C (void);
// 0x00000154 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF (void);
// 0x00000155 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A (void);
// 0x00000156 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D (void);
// 0x00000157 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0 (void);
// 0x00000158 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B (void);
// 0x00000159 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363 (void);
// 0x0000015A System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C (void);
// 0x0000015B System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B (void);
// 0x0000015C System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE (void);
// 0x0000015D System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943 (void);
// 0x0000015E System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD (void);
// 0x0000015F System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0 (void);
// 0x00000160 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66 (void);
// 0x00000161 System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF (void);
// 0x00000162 System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A (void);
// 0x00000163 System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB (void);
// 0x00000164 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781 (void);
// 0x00000165 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910 (void);
// 0x00000166 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691 (void);
// 0x00000167 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D (void);
// 0x00000168 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F (void);
// 0x00000169 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F (void);
// 0x0000016A System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC (void);
// 0x0000016B System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2 (void);
// 0x0000016C System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33 (void);
// 0x0000016D System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E (void);
// 0x0000016E System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944 (void);
// 0x0000016F System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749 (void);
// 0x00000170 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647 (void);
// 0x00000171 System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84 (void);
// 0x00000172 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC (void);
// 0x00000173 System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB (void);
// 0x00000174 System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF (void);
// 0x00000175 System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE (void);
// 0x00000176 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7 (void);
// 0x00000177 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF (void);
// 0x00000178 System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629 (void);
// 0x00000179 System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9 (void);
// 0x0000017A System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715 (void);
// 0x0000017B System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B (void);
// 0x0000017C System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8 (void);
// 0x0000017D System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261 (void);
// 0x0000017E System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0 (void);
// 0x0000017F System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA (void);
// 0x00000180 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50 (void);
// 0x00000181 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712 (void);
// 0x00000182 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8 (void);
// 0x00000183 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534 (void);
// 0x00000184 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4 (void);
// 0x00000185 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848 (void);
// 0x00000186 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D (void);
// 0x00000187 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C (void);
// 0x00000188 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3 (void);
// 0x00000189 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83 (void);
// 0x0000018A System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691 (void);
// 0x0000018B System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F (void);
// 0x0000018C System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0 (void);
// 0x0000018D System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8 (void);
// 0x0000018E System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210 (void);
// 0x0000018F System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB (void);
// 0x00000190 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F (void);
// 0x00000191 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27 (void);
// 0x00000192 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046 (void);
// 0x00000193 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1 (void);
// 0x00000194 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7 (void);
// 0x00000195 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF (void);
// 0x00000196 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E (void);
// 0x00000197 System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8 (void);
// 0x00000198 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B (void);
// 0x00000199 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C (void);
// 0x0000019A System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28 (void);
// 0x0000019B System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963 (void);
// 0x0000019C System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8 (void);
// 0x0000019D System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B (void);
// 0x0000019E System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20 (void);
// 0x0000019F System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3 (void);
// 0x000001A0 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A (void);
// 0x000001A1 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598 (void);
// 0x000001A2 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5 (void);
// 0x000001A3 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6 (void);
// 0x000001A4 System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB (void);
// 0x000001A5 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA (void);
// 0x000001A6 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1 (void);
// 0x000001A7 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC (void);
// 0x000001A8 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66 (void);
// 0x000001A9 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42 (void);
// 0x000001AA System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9 (void);
// 0x000001AB System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F (void);
// 0x000001AC System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007 (void);
// 0x000001AD System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6 (void);
// 0x000001AE System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51 (void);
// 0x000001AF System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932 (void);
// 0x000001B0 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52 (void);
// 0x000001B1 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F (void);
// 0x000001B2 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825 (void);
// 0x000001B3 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846 (void);
// 0x000001B4 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97 (void);
// 0x000001B5 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549 (void);
// 0x000001B6 System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16 (void);
// 0x000001B7 System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE (void);
// 0x000001B8 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8 (void);
// 0x000001B9 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099 (void);
// 0x000001BA System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B (void);
// 0x000001BB System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479 (void);
// 0x000001BC System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D (void);
// 0x000001BD System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF (void);
// 0x000001BE System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2 (void);
// 0x000001BF System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA (void);
// 0x000001C0 System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2 (void);
// 0x000001C1 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE (void);
// 0x000001C2 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E (void);
// 0x000001C3 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599 (void);
// 0x000001C4 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62 (void);
// 0x000001C5 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2 (void);
// 0x000001C6 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81 (void);
// 0x000001C7 System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F (void);
// 0x000001C8 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE (void);
// 0x000001C9 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA (void);
// 0x000001CA System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E (void);
// 0x000001CB System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5 (void);
// 0x000001CC System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971 (void);
// 0x000001CD System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4 (void);
// 0x000001CE System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1 (void);
// 0x000001CF System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5 (void);
// 0x000001D0 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87 (void);
// 0x000001D1 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC (void);
// 0x000001D2 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760 (void);
// 0x000001D3 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC (void);
// 0x000001D4 System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E (void);
// 0x000001D5 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D (void);
// 0x000001D6 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB (void);
// 0x000001D7 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD (void);
// 0x000001D8 System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C (void);
// 0x000001D9 System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC (void);
// 0x000001DA System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884 (void);
// 0x000001DB System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C (void);
// 0x000001DC System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F (void);
// 0x000001DD System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED (void);
// 0x000001DE System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2 (void);
// 0x000001DF System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348 (void);
// 0x000001E0 System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107 (void);
// 0x000001E1 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2 (void);
// 0x000001E2 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F (void);
// 0x000001E3 System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66 (void);
// 0x000001E4 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9 (void);
// 0x000001E5 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C (void);
// 0x000001E6 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F (void);
// 0x000001E7 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0 (void);
// 0x000001E8 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4 (void);
// 0x000001E9 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB (void);
// 0x000001EA System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2 (void);
// 0x000001EB System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099 (void);
// 0x000001EC System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49 (void);
// 0x000001ED System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60 (void);
// 0x000001EE System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84 (void);
// 0x000001EF System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1 (void);
// 0x000001F0 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535 (void);
// 0x000001F1 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25 (void);
// 0x000001F2 System.Void UnityEngine.Tilemaps.AnimatedTile::GetTileData(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileData&)
extern void AnimatedTile_GetTileData_mD611724E4E114405C9177FF5E3C03A32570F3AFC (void);
// 0x000001F3 System.Boolean UnityEngine.Tilemaps.AnimatedTile::GetTileAnimationData(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileAnimationData&)
extern void AnimatedTile_GetTileAnimationData_m8655D1439CA8A7F8C8B036EF3C8E1988426A07CF (void);
// 0x000001F4 System.Void UnityEngine.Tilemaps.AnimatedTile::.ctor()
extern void AnimatedTile__ctor_mD8826B5666F0E9DC2AE8C0BA71902EBB5A0A28CF (void);
// 0x000001F5 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD94B0E22EF32AD3DFD277ED8E911B5DFA4CDB91E (void);
static Il2CppMethodPointer s_methodPointers[501] = 
{
	Bug_Start_mC486A11B805E5F181C57FB7FC1908E308176C4C4,
	Bug_Update_mB67BCAA4E66B107438CF06A3FE3D412041B8ED3B,
	Bug_FixedUpdate_m47FFFA9E2C5A90A79C23C2A87DF32453F46B40E0,
	Bug_OnCollisionEnter2D_m2CE3B919EABFD4E6BA5D00235F9C5918A64240AF,
	Bug_OnTriggerStay2D_mCEBBF08F6B8F4FD7340C9A1FD3E152E25C09D2A8,
	Bug_OnTriggerExit2D_m7792D0B1D809DD72911CD40056458B2F647154FD,
	Bug_ResetPickup_m314CB1A7C7B893D01CF52559CBBDE89E054A0810,
	Bug__ctor_mAEFF79F71DBF4B6C9BFFF1D2DABB336DB6110F57,
	BugLog_Start_mB8448229845ED2D12A630EA233192B27E55C20D8,
	BugLog_Update_mFEFE813679327D37E4B5F4202F092013365CADB0,
	BugLog_UpdateLevel_m63BED2555F63579FC3024C41669AEE1E3CEB1EB9,
	BugLog_AddBug_m05179868B25C0F7A32AB9DC44B2576DB528C7341,
	BugLog_GetBugCount_m17ADF86EF09DE45B5EE744455BAD25908CAF21B2,
	BugLog_GetBugLevel_m88AC8D9DB1FA2EE1283CD86DA2A980A799362FEF,
	BugLog_isBugged_mCDF827C888D32AEE4CFB53936E22BE6E07D5BDE0,
	BugLog__ctor_m9C7EA3327E58B41B2F1F5E021123F6962D60A309,
	Bullet_Start_m58181B46F80FE1ABECE1AFF455C253B51B7EB0E4,
	Bullet_Update_mB82408CA535D8533168045E7C0321090448B596B,
	Bullet_FixedUpdate_m2CED3258420C0BB71A2CE12F3E0C0287A8D72145,
	Bullet_SetSpeed_m50BFFC7C609F4ACD0A9CA122DF5D47A039223932,
	Bullet_SetDirection_m04213FA5AABE357B0C567FA2F5AB5DC3FEE4BCAF,
	Bullet_SetPower_m2ED5613FFE53994BAA2E7F2DDDA7982063170384,
	Bullet_GetVelocity_m68CC73C2858FC7F552CF400321CFDD49E04C991F,
	Bullet_SetGravity_m478DCD844E2E22F610B65A2A8B4D0D588BFE4F64,
	Bullet_SetDamage_m6A6975C68C1F685D519FF14644FCB9B10B4FF906,
	Bullet_GetDamage_m9CA04D892B75A0D99E7A8AFEA83D34FCA1487599,
	Bullet_OnCollisionEnter2D_m4CC80D190EE3A6B6E2EDA524A121942D0285D0A9,
	Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC,
	CharacterController2D_Awake_mFD21C000EE8097373F734648EAC2BFDB901197FA,
	CharacterController2D_FixedUpdate_m00D8CBF2991777D0875EB1D13AF8222831A2F0E8,
	CharacterController2D_Move_mE541579D7B4A796A34C1E2AFF0DAD5DEADD475F9,
	CharacterController2D_Flip_m8F1D0D7A87D7E507031BBCDF6C0DB095AFB21965,
	CharacterController2D__ctor_m5C93DA812470EF6163E25545DED6CA554BBA83F7,
	BoolEvent__ctor_mB1C4B8CED2296E60F44636855900EA1BDF1F393A,
	DamageIndicator_Start_m4B11F0BBDA0E041B7CC3D21229D415AF5C8C4EF4,
	DamageIndicator_Update_m464572748BC80B633E7824A9866A9E47BE399184,
	DamageIndicator_SetText_mF97716CDCCE4E2B072FB54D8EEE63ABB6AFD72B7,
	DamageIndicator_SetTextAndTimer_m78D55BA6F64B7BE16EAB69F3BF17D728A4D47906,
	DamageIndicator__ctor_m829527469E0E1B01175A96EDB17FDEB95236C962,
	King_Start_m69981586CA9F43BF63B90B6B3CE94DE7D5887F18,
	King_Update_m5791314117CB9A57DEB4AE524E23B3B27E491BA6,
	King_OnTriggerEnter2D_mF632A4325E96D06F490F6EC99546591793E3D600,
	King_OnTriggerStay2D_mD29D1D718F19E886C850EBFCE6724E10D16673FA,
	King_Attack_m563523658F2ABD9B8E0FAD48BE85DFF918308AD6,
	King_TakeDamage_m29A678073F6207C6B7AF5F53A46A7250325EFC8E,
	King_OnDeath_m4C854BC2DF9B102972DA734568C19ECF487E27A8,
	King__ctor_m928AA3E86E3C2071EECB6DCD071F40251F6D0E54,
	Looter_Start_m2318EA0F070BC32FD5463CCA485FE52EA5CEDCF1,
	Looter_Update_mA497B96458912A20F5489F7FD33C3EFAEBEE2CD3,
	Looter_FixedUpdate_m1C3BC021A5129008AB0EF8FCF46CE56781C29220,
	Looter_OnTriggerEnter2D_m23B9D20A99CE4FFACC27539A2D52896E250D85B8,
	Looter_OnTriggerStay2D_mAAFD2215D7B7C6ABEC53DDAEAF2CD4DAF88BA2A6,
	Looter_OnTriggerExit2D_mC65EEBF9E2F01AF14477BCD539BE97B3B3C53986,
	Looter_GetNextPosition_mACFA8A5A1B0F1ABD5A9B6690ABDCA7B77745D2C9,
	Looter_TakeDamage_mE68A87219643A040ECE336B4BE1C359ACF3ECB13,
	Looter_SetHealth_mB9657F33D11B013B2089CCD283EC2ED0057404BC,
	Looter_OnDeath_m452AF4C76398A61937EB7EC3C234C3AF25D98F64,
	Looter__ctor_m6E133B7DA662A570549B173EFF93C938D8DFB0BA,
	MainMenu_PlayGame_m96A3CE2743BCB00B665AA3AC575AE4EBD9ED40B0,
	MainMenu_QuitGame_m9F32E266C6F6CE345067D062258362159D267030,
	MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D,
	Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021,
	Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3,
	Player_FixedUpdate_mD7447EDFC86F29A3E5FBDEF7E0139535BD4C5088,
	Player_OnCollisionEnter2D_m0448BB6F02AAB4967D3AB976DA22A24ACD8E7F90,
	Player_OnCollisionStay2D_m81D8A16E9CB60D15F075E7A339EF5FF98475D680,
	Player_TakeDamage_m11E35C8E11AB669B270A7498623D779590182DFF,
	Player_Attack_mDBF5675D49B5FEAF3B69717FA732B2FCAC54AB98,
	Player_GetDealtDamage_mE510DA7BBF42112779340233BC1DF6475B0E59D0,
	Player_Stop_m1C3A5D2B110766366C98792A0E80BCE220554B3E,
	Player_QuitApp_m89E1ABAE0B609D6B2FE68795D2DC97FD9234E1E5,
	Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7,
	Princess_Start_m53049A703127B9DC0045AFA68F0B4172699759A0,
	Princess_Update_mC4B9B858694510B7BE89078C12B980CF9CB8A0CF,
	Princess_FixedUpdate_mF45DE393E3B4D5BA8C3BB3D30909F1F91173EAA1,
	Princess_OnTriggerEnter2D_m238228C39DE13426DBD3D2D2485CBB2B8EE226E3,
	Princess_OnTriggerStay2D_m51F88039C8B9770EC38F296D0914161E77984EA5,
	Princess_OnTriggerExit2D_m7D64A1E1F6B0727C0320FE0B81E003C09C66027F,
	Princess_OnCollisionEnter2D_m01C2B0AE0F75373BBDFBD98FCCAEBC78BDBB96B3,
	Princess_TakeDamage_mDA45461DC925A552C072DA18031F00B1CA0E0838,
	Princess_SetHealth_mFDFF208312C2D18A5A7851EA01770EE06A6CFA62,
	Princess_OnDeath_mC7EF0DC5318CADA6AF3D7D5D69105F184496269C,
	Princess_Panic_mE25CDB1E442F2C1A479C0061CAB65B2236A9895A,
	Princess__ctor_m36185E3125E99DE33380F8D4ED4D58BB7C84E20D,
	Runner_Start_mD09A99E8C2108694F858656063D1E3F62B5C2373,
	Runner_Update_m52DE8D69CFDBFBA32D8E861563463E64C6247B19,
	Runner_FixedUpdate_m5ACC762150AC6CCD90809EE5786EB08E175D6F42,
	Runner_OnTriggerEnter2D_mF3A121ABFE6BC3A6CDCB5405A628F07694900E20,
	Runner_OnTriggerStay2D_mEEDB68CFBE21FBBF0D22FDEE3B10CC09824000BA,
	Runner_OnTriggerExit2D_mE8EBB886C33465195CBC2A57D62D1A39C045EC5A,
	Runner_TakeDamage_m9BBCEDD903FCCDA5C746F822C9B00D6B7BE89B1A,
	Runner_SetHealth_mC817B3E4C747FD01FA8A0ECFCFCC4562CFE58851,
	Runner_OnDeath_m9314366CAA8AAC09F9AEBE40D21583DACDF53564,
	Runner_Panic_m03F977FB97B10BA32CACF139A86A4A43911CB49A,
	Runner__ctor_m90AC232C165056FBD851D53F1689BD5FC15563FA,
	Shooter_Start_mBA83984BC9920FA1D7FF4B5F60389CC102F8CE77,
	Shooter_Update_mF95D0E122742F65FF3236CC03DDE9CA17DDCB7E2,
	Shooter_FixedUpdate_mAE5EF3A229F96A18D817674F378479A86BA0CB1F,
	Shooter_OnTriggerEnter2D_mACB5436967FD849BB98149421C5C2600B5CC92F4,
	Shooter_OnTriggerStay2D_mB9D2D282C2FBAD9213499D47A3B7E735DF4EAF1D,
	Shooter_OnTriggerExit2D_mF40AC4779682CFCD3A6D2B91F8180B9B1A42197C,
	Shooter_get_ShootToKill_m8FF035DCEB12C201A7F91678A26464F55440CD67,
	Shooter_set_ShootToKill_m5A910F98A663E778F8C3E14AD6A18C34CFD0A3FB,
	Shooter_GetShootDirection_m863DA4A194D3695648318F8E0274848F7787B7B8,
	Shooter_SetShootDirection_m57E3C5933CF47E219CEE623AC6D994C8EFE615B5,
	Shooter_get_ShootRate_m13A64EFA275A9AA5432951838302AA1BC668D6DA,
	Shooter_set_ShootRate_mC1750D830B325B9E82D7EE73C294A66AD80EFC1B,
	Shooter_get_ShootSpeed_m68331A226E5CD07EF49F80E6BABACD7566632BC9,
	Shooter_set_ShootSpeed_m33024A121B7BCBC28302569586D0DA41EA94F403,
	Shooter_TakeDamage_m61D297CBAC0ADC70E7F37CB261A157D43E663285,
	Shooter_SetHealth_m1FA2DC71118E52EA760BEB115DC02EC889B37ABD,
	Shooter_OnDeath_mA53E550258EA3475F42F809088DE6BE100BE33F5,
	Shooter__ctor_m173C774BF5668018A0B10ACA4BB5A6DA3A6BAF2D,
	Spawner_Start_m61D97BD980BD1B1877634A1E7626E47418D5D6D8,
	Spawner_Spawn_m9A3438B8559369C0BC1578C2AE341CCA181E6B6C,
	Spawner_GetSpawnRadius_m0B0717C35412E6DEDE11B995E35F734D500834EC,
	Spawner_GetTotal_mD93FFF5C48556D87C319267B0AAEA9C13E1929E6,
	Spawner__ctor_m08E8D40AAA40F4329D8A95EEE2B2B6BE842CEB9C,
	Spike_Start_m60979F603D0D0A5A73706AD7FF71B8BA14FE64A3,
	Spike_Update_m130B8AC280A3390D6F67CEFC28C4C7E7B0ADFA49,
	Spike__ctor_m5908FB939F6D9A6FBFE73C02127CF54079DA8BB1,
	Trigger_Start_m3A9539F692683C0656862AF715A3155BE7235812,
	Trigger_OnTriggerExit_mD9036B187B8A47B94E9F03CF4287F0DFFF99C8CF,
	Trigger_OnTriggerEnter_mBC7228041960CB9DB4ED33DBA9513A8DAFDC862A,
	Trigger__ctor_m42036DF1067B2973F4CF5F07DF83F4162A21081C,
	UI_Text_Start_m7FD933C09A4FB9ECCC5D261D1AED22E344101C48,
	UI_Text_Update_m05F21BDD61C4B88D88C5AAB124772A6CE47F9D0E,
	UI_Text_SetText_m21691663AC1558A788D56FC9C47F263D250A0235,
	UI_Text_SetColor_m7E211B828CA484914CEBE7F108E7447410561B1E,
	UI_Text_SetTextAndTimer_m8EEF10F93362E077EE68862D75C2255533698011,
	UI_Text__ctor_m5932DBE70BF136736B8B9A21A1407381B3B020D5,
	Demo_Start_mA96D71BE495F276B026F0223EAAF21A1B4C626E8,
	Demo_Click_m3027181571AD1AC23FE4E3C7A0C03F5E21983966,
	Demo_TogglePause_m1B9D3936E6A3A4B8FA372E0C5184FF9D32C2A7C0,
	Demo_OnEnable_m18912F888C952F5A063CE0A6A3A9F57C3A05314C,
	Demo_OnDisable_mF53457E7F2E00A5AC6FBA9A5526150DB5550DDBA,
	Demo__ctor_mA1BCCF87BB86D1772EABA9E89B452959AA579645,
	SMSound_GetVolume_mC43AED7BE161373FD58012DB293D81EAD770989A,
	SMSound_SetVolume_m58AA414EDCBCB36BA262F80055F0153CFEC86F5B,
	SMSound_SetLooped_m913D374DA0D8764D112FBDF7BA4450F5E42F756B,
	SMSound_SetPausable_m43D52A214E7B163226BDCCEA64E428CAAEFE34F3,
	SMSound_Set3D_m3807F763F5091B98C6525BDCDAF9A22CCAA0C836,
	SMSound_AttachToObject_m7B3BD9B16D547C25CDD544FB4F483542A926CBF6,
	SMSound_SetPosition_m04B12FF50E47B12AF7458778206DE27AE9280A8D,
	SMSound_Stop_m4CDC150347B24264A8436F331CB139CBCF1C4BDC,
	SMSound__ctor_m4C279C963F40290FECD10E090D673B0EB81ABAC6,
	SoundManager_PlayMusic_m6251F4C75B16CFD4581C0C63F3CDAC4C2E0D0694,
	SoundManager_StopMusic_mECA9EABD5243BD1D37D962FBBCA619055563411A,
	SoundManager_PlaySound_m587A71005C68154DD9ED8003394F77B87266EB8E,
	SoundManager_PlaySoundUI_mB8E07596B5F136248D0D3FBD5361E8AFA71EA3A9,
	SoundManager_PlaySound_m0C148519D3AED0D81A139EF621A4C5CFAF10AEE5,
	SoundManager_PlaySoundUI_mC929F5103AF02FBD9F3FD10EA93F6F6F8A18297E,
	SoundManager_PlaySound_m67F215B6697BE0670B85D53785476B8989799CBA,
	SoundManager_PlaySoundUI_mED1AA4F61D0C832C1750733C5B6B0357E4530B4F,
	SoundManager_PlaySoundWithDelay_m07CC473A3F7F4D29225B16E5465E1E3947FF0590,
	SoundManager_LoadSound_m56A3465AA637ACC641C819B00224CC3903D5D1BA,
	SoundManager_UnloadSound_mAA549E53194BD1A0A610EEB36998F5FB4A1BF5EA,
	SoundManager_Pause_mCE7BD62E35460BB4C8B4C0289BA55F34ADD6D4DC,
	SoundManager_UnPause_m478BB470970DDC471BFB25D98D5D4EE934224823,
	SoundManager_StopAllPausableSounds_mA4C1EED3EA9463FF48803FCD2B07A87390A4C4C6,
	SoundManager_SetMusicVolume_m3DCEA13F93A1885620C5A0239B1F225D52C6EDC0,
	SoundManager_GetMusicVolume_m8A00ACEB320E2DC02BB1012794432F5BCC800D76,
	SoundManager_SetMusicMuted_m6AEE27FAD4AD5F2D8B0F8AEE61A0B1585BD2BDBF,
	SoundManager_GetMusicMuted_mE0084F382B3DAFF61FC98F3DA3665C2E81F61337,
	SoundManager_SetSoundVolume_m6600DFCCAF9F424D4D5C5AB545B9D7CCD703B48C,
	SoundManager_GetSoundVolume_m3AE9165E1F72BB65442B1F4E4E09A91C32198DE4,
	SoundManager_SetSoundMuted_mCF38F4FC1705C73B33A0E95FB76A358312644ED0,
	SoundManager_GetSoundMuted_mE08B2FC4C79D52ED2A878EBD6386BD658E08BED1,
	SoundManager_IsValid_m6FB686E39EC39949F1BE858F38294D93117D1FBF,
	SoundManager_GetSettings_m877365E28CAA00980917E0E52E41E54934AA431A,
	SoundManager_Stop_m782738B5E79C91EF3E34D56C5428AE1D2DF6F8F7,
	SoundManager_get_Instance_mB2D05669AF91B4D14A63BC76820A362B6F36AB90,
	SoundManager_OnDestroy_m251DAD37E7BEDCEA021D39A9E132CF610BF43FBF,
	SoundManager_PlayMusicInternal_mE9BA4EC365B0FEAFB096F4443E19C9BD76DF68BE,
	SoundManager_StopMusicInternal_mA18857574BB02C6AF1BD8994CD3FC880D0CF0A58,
	SoundManager_PlaySoundInternal_mAA60F2DB6288927FCDE27CA874DDB6C68676A65D,
	SoundManager_PlaySoundClipInternal_m85A8840CB6CC5264B9BA9B08B118609904F17F8F,
	SoundManager_PlaySoundInternalAfterLoad_m69CE512AB33E3672FC4DB3E26529D4AD2E9A1B88,
	SoundManager_PlaySoundWithDelayInternal_m07DD8B3B99F9255D3AB8F73EE27374209B1D7A04,
	SoundManager_StopAllPausableSoundsInternal_mAAD00174BF7D34434314E32CAFE22B2E0BB137BD,
	SoundManager_StopSoundInternal_mF12FAB38DDC1F46F9CEAE028357CE35AD9C67706,
	SoundManager_LoadSoundInternal_mE69272F4A3F06CC62F914050EC859F5AE3AE70D5,
	SoundManager_UnloadSoundInternal_m968B8AA16ECD0E4B18EE4AAD59D6F8AD349DF7E8,
	SoundManager_Awake_m78F953F39CFB3F539240E1226D04270B793B1A76,
	SoundManager_Update_mDD188B65FF1E9B1DF1B8345A6290D149E70E657C,
	SoundManager_LateUpdate_m76E0C0FA7F82739334A4F04E457F47B08C971719,
	SoundManager_StartFadingOutMusic_m2296A245F1A0255B82AAF14FC7AA41BCBA9DA17B,
	SoundManager_PlaySoundWithDelayCoroutine_m0110575D6B5522AE00F1613CC6EA6D75E12233DF,
	SoundManager_LoadClip_m99E0190157845EC162ADDFAD1F0EFBDDC9B3FF89,
	SoundManager_LoadClipAsync_mCCFE6DF970CA8D3B19626B2C1F08F5E44AC630EB,
	SoundManager_LoadClipFromBundleAsync_m7DB5E73164ED90CB63E7BA0128F2DF341F0C6919,
	SoundManager_IsSoundFinished_mFAA2F4D8C7E0C6E2BFA4AC49B7735EE28A4FE3F1,
	SoundManager_ApplySoundVolume_m7CAB25628C9F40061943E554BA349057A3BF501F,
	SoundManager_ApplyMusicVolume_mECB66840BEAFC21585E4D79C55264D93912C9C65,
	SoundManager_ApplySoundMuted_mA17F5B0880B023141ED24C0D79BC6EED7920E133,
	SoundManager_ApplyMusicMuted_m45D3B059FF30F80FC1EB0CD63200205FD5FA8392,
	SoundManager__ctor_m30D18BC25871BD3FF7FB195A1CAE50A2EE48FCAE,
	SoundManager__cctor_mCB4F772852263D66AF3536FB81B715BD441B6A2C,
	SMMusic__ctor_m5EDCBB5468C954967E0F6F0C614585120C60E7C5,
	SMMusicFadingOut__ctor_m8FE1B268D59CE43E83B52E0CEF90E32DD734929E,
	U3CPlaySoundInternalAfterLoadU3Ed__45__ctor_mD6064B0F2DDD2C4935997AF85C3A1930245D20E7,
	U3CPlaySoundInternalAfterLoadU3Ed__45_System_IDisposable_Dispose_mD9A491C7F3B9F7B5075D2C2948D7BE2F94B689A5,
	U3CPlaySoundInternalAfterLoadU3Ed__45_MoveNext_mEE0760C84CDD8948D7FB13FA7033AAA83185F8A1,
	U3CPlaySoundInternalAfterLoadU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE414256812BA6540771F8F9B5EA2721869F2D183,
	U3CPlaySoundInternalAfterLoadU3Ed__45_System_Collections_IEnumerator_Reset_m30A5FDEC90A34FE45B02AAF2DA653A294C2C7698,
	U3CPlaySoundInternalAfterLoadU3Ed__45_System_Collections_IEnumerator_get_Current_m8C7F70D3636F110215CA91C2B44EB6C503CAA24A,
	U3CPlaySoundWithDelayCoroutineU3Ed__55__ctor_mCB5E5B2BE111444B588F31BA0C5AFD7509B69712,
	U3CPlaySoundWithDelayCoroutineU3Ed__55_System_IDisposable_Dispose_m7817CCF7616CF50FF520EE581D0CA567D3380903,
	U3CPlaySoundWithDelayCoroutineU3Ed__55_MoveNext_m40B6DF85C7E6F41AB7ECCA7CC2AF223EBCF24597,
	U3CPlaySoundWithDelayCoroutineU3Ed__55_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC01FA9B1E453E98F34D9ACC6366AEAA36AAAC5B4,
	U3CPlaySoundWithDelayCoroutineU3Ed__55_System_Collections_IEnumerator_Reset_m51984042EB5E01171299361F7E1DCFF91D5E5C35,
	U3CPlaySoundWithDelayCoroutineU3Ed__55_System_Collections_IEnumerator_get_Current_mD113099C5BD2CEF9A9D657CD45880CB128609A23,
	SoundManagerComponent_PlaySound_mFBA43D406AB3B7E122C732BD2488746076D521C0,
	SoundManagerComponent_PlaySoundNotPausable_mAAF03ACECC1FADC86DCE3FF6540541D8CE5F4A71,
	SoundManagerComponent_ChangeSoundVolume_m9C2A069197722F8323FF30AF7FEDFFD7AB9A792B,
	SoundManagerComponent_ChangeMusicVolume_mCAF30EDB0D5A6CF85043FE191B228796FFF6E797,
	SoundManagerComponent_ToggleMusicMuted_m3FB62421AC201F9DCFA9A5912B71612BE74CE7FB,
	SoundManagerComponent_ToggleSoundMuted_m6F317B59789580E114851CEAB638114D74552B2D,
	SoundManagerComponent__ctor_m1EFC625BD85A6AC60B68532BB3E3B3A8339865C8,
	SoundManagerSettings_SaveSettings_m0E8B6E03EBA5A0520B468788211CCCF3E82CCD33,
	SoundManagerSettings_LoadSettings_m171B6F4C319015EB3FE75F18BA130B23CC8EC594,
	SoundManagerSettings_SetMusicVolume_m0F4DF6F9A63C77149C24B57AA241ACD5EE115698,
	SoundManagerSettings_GetMusicVolume_m20193DF9974CEE67FE71CD1165DBAB2076A0E8F9,
	SoundManagerSettings_SetSoundVolume_mD30B9C446781EEF76D2FF2066192E7AF58FAF390,
	SoundManagerSettings_GetSoundVolume_mAC515D97E233056D18F56065B7B020EA05CBC8BC,
	SoundManagerSettings_SetMusicMuted_m8A98932A4C78C4DF6F508E656F4099727A1763F7,
	SoundManagerSettings_GetMusicMuted_m4FBFB0E680A90C4498C4C53864DA0B16445FDB5A,
	SoundManagerSettings_SetSoundMuted_m50610F4FDCA933C111F797ECB692CE5CCA05B88E,
	SoundManagerSettings_GetSoundMuted_m5EB7F422BCEBDED4A082DF639F084194169A60EE,
	SoundManagerSettings_GetSoundVolumeCorrected_m92A031C60B6497D6AEF654419ABA27693CEA3C2F,
	SoundManagerSettings_GetMusicVolumeCorrected_m8DAD6A1342EED1DB3CF50F0BDA3788E0E29E6F90,
	SoundManagerSettings__ctor_m65F81B28B6C9D067D362C71F1691085F078D00B4,
	ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2,
	ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B,
	ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371,
	ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026,
	DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6,
	DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9,
	EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93,
	EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC,
	U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226,
	TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E,
	TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1,
	TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3,
	TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385,
	TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4,
	TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B,
	TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788,
	TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A,
	TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076,
	TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D,
	TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA,
	TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB,
	TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5,
	TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70,
	TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11,
	TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC,
	TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44,
	TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC,
	TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53,
	TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66,
	TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43,
	TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77,
	TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8,
	CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7,
	SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2,
	WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C,
	LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018,
	LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0,
	Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F,
	U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376,
	U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046,
	Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5,
	Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77,
	Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516,
	Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE,
	Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9,
	Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C,
	CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E,
	CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610,
	CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373,
	CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3,
	CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124,
	ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA,
	ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3,
	ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC,
	ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8,
	ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2,
	ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64,
	U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F,
	SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5,
	SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697,
	SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB,
	SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F,
	SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3,
	SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86,
	U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4,
	TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB,
	TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12,
	TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3,
	TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720,
	TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21,
	TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1,
	TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C,
	TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF,
	TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A,
	TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D,
	TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0,
	TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B,
	TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363,
	TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C,
	TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B,
	TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE,
	TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943,
	TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD,
	TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0,
	TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66,
	TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF,
	TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A,
	TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB,
	TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910,
	TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691,
	TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D,
	TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F,
	TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F,
	TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2,
	TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33,
	TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E,
	TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944,
	TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647,
	TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84,
	TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB,
	TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF,
	TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE,
	TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF,
	U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0,
	TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA,
	TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50,
	TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712,
	TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8,
	TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848,
	U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046,
	TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E,
	TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6,
	TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB,
	TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA,
	TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1,
	VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC,
	VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66,
	VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F,
	VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825,
	VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846,
	VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97,
	VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549,
	VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE,
	U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2,
	VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA,
	VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2,
	VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE,
	VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E,
	VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62,
	U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5,
	VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971,
	VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4,
	VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1,
	VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5,
	VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC,
	U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C,
	VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC,
	VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884,
	VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C,
	VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F,
	VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2,
	U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107,
	U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0,
	WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4,
	WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB,
	WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099,
	U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
	AnimatedTile_GetTileData_mD611724E4E114405C9177FF5E3C03A32570F3AFC,
	AnimatedTile_GetTileAnimationData_m8655D1439CA8A7F8C8B036EF3C8E1988426A07CF,
	AnimatedTile__ctor_mD8826B5666F0E9DC2AE8C0BA71902EBB5A0A28CF,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD94B0E22EF32AD3DFD277ED8E911B5DFA4CDB91E,
};
static const int32_t s_InvokerIndices[501] = 
{
	1969,
	1969,
	1969,
	1631,
	1631,
	1631,
	1969,
	1969,
	1969,
	1969,
	1945,
	1405,
	1906,
	1906,
	1418,
	1969,
	1969,
	1969,
	1969,
	1652,
	1652,
	1618,
	1964,
	1650,
	1618,
	1906,
	1631,
	1969,
	1969,
	1969,
	645,
	1969,
	1969,
	1969,
	1969,
	1969,
	1631,
	1631,
	1969,
	1969,
	1969,
	1631,
	1631,
	1969,
	1618,
	1969,
	1969,
	1969,
	1969,
	1969,
	1631,
	1631,
	1631,
	1969,
	1618,
	1618,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1631,
	1631,
	939,
	1969,
	1906,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1631,
	1631,
	1631,
	1631,
	1618,
	1618,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1631,
	1631,
	1631,
	1618,
	1618,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1631,
	1631,
	1631,
	1945,
	1650,
	1947,
	1652,
	1947,
	1652,
	1947,
	1652,
	1618,
	1618,
	1969,
	1969,
	1969,
	1969,
	1947,
	1947,
	1969,
	1969,
	1969,
	1969,
	1969,
	1631,
	1631,
	1969,
	1969,
	1969,
	1631,
	1585,
	1631,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1947,
	1289,
	1288,
	1288,
	1288,
	1287,
	1291,
	1969,
	1969,
	3070,
	3122,
	2980,
	2980,
	2701,
	2701,
	2980,
	2980,
	2602,
	3070,
	2856,
	3122,
	3122,
	3122,
	3077,
	3115,
	3074,
	3113,
	3077,
	3115,
	3074,
	3113,
	3113,
	1920,
	1631,
	3104,
	1969,
	1631,
	1969,
	477,
	733,
	474,
	627,
	1969,
	1631,
	1631,
	1008,
	1969,
	1969,
	1969,
	1969,
	479,
	1287,
	1287,
	732,
	1418,
	1969,
	1969,
	1969,
	1969,
	1969,
	3122,
	1969,
	1969,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	1631,
	1631,
	1652,
	1652,
	1969,
	1969,
	1969,
	1969,
	1969,
	1652,
	1947,
	1652,
	1947,
	1650,
	1945,
	1650,
	1945,
	1947,
	1947,
	1969,
	1969,
	1969,
	1631,
	1969,
	1969,
	1969,
	1969,
	1920,
	1969,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	430,
	1969,
	430,
	1969,
	1920,
	1631,
	1920,
	1631,
	1920,
	1631,
	1920,
	1631,
	1920,
	1631,
	1969,
	1969,
	1631,
	1631,
	853,
	853,
	605,
	605,
	613,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1920,
	1969,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	1920,
	1969,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1920,
	1969,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	1969,
	1969,
	1969,
	1969,
	1969,
	1287,
	1920,
	1969,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	1969,
	1969,
	1969,
	1969,
	1969,
	1969,
	1618,
	1969,
	1969,
	1969,
	853,
	853,
	605,
	605,
	613,
	1969,
	1969,
	1969,
	1969,
	1631,
	1631,
	1969,
	1969,
	1969,
	1969,
	1631,
	1969,
	1631,
	1631,
	1631,
	1631,
	1618,
	1969,
	1969,
	1969,
	1969,
	1618,
	1969,
	1969,
	1618,
	1969,
	1969,
	1920,
	1969,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	1969,
	1969,
	1969,
	1969,
	1631,
	1287,
	1287,
	1969,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	1969,
	1969,
	1920,
	1920,
	1969,
	3122,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	1969,
	1969,
	1969,
	1969,
	1969,
	1920,
	1969,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	1969,
	1969,
	1969,
	1969,
	1631,
	1920,
	1969,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	1969,
	1969,
	1969,
	1969,
	1631,
	1920,
	1969,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	1969,
	1969,
	1969,
	1969,
	1631,
	1920,
	1969,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	1969,
	1969,
	1969,
	1969,
	1631,
	1920,
	1969,
	1969,
	674,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	1969,
	1969,
	1287,
	1920,
	1969,
	1618,
	1969,
	1945,
	1920,
	1969,
	1920,
	653,
	551,
	1969,
	2940,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	501,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync(System.String)
// 0x00000002 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync(System.String,System.Type)
extern void AssetBundle_LoadAssetAsync_m63C7C5654FA8D0824DC920A1B1530C37CCB3DF6E (void);
// 0x00000003 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync_Internal(System.String,System.Type)
extern void AssetBundle_LoadAssetAsync_Internal_m89D45EFB48D3C4CF7E481EF41F7ACF24500E7019 (void);
// 0x00000004 UnityEngine.Object UnityEngine.AssetBundleRequest::GetResult()
extern void AssetBundleRequest_GetResult_m792F2C703230D18A0B6B18C86636961125B1B9A2 (void);
// 0x00000005 UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern void AssetBundleRequest_get_asset_mB0A96FBC026D143638E467DEB37228ACD55F1813 (void);
// 0x00000006 System.Void UnityEngine.AssetBundleRequest::.ctor()
extern void AssetBundleRequest__ctor_mD09AF030644EF7F3386ABB3B5C593F61ADE25017 (void);
static Il2CppMethodPointer s_methodPointers[6] = 
{
	NULL,
	AssetBundle_LoadAssetAsync_m63C7C5654FA8D0824DC920A1B1530C37CCB3DF6E,
	AssetBundle_LoadAssetAsync_Internal_m89D45EFB48D3C4CF7E481EF41F7ACF24500E7019,
	AssetBundleRequest_GetResult_m792F2C703230D18A0B6B18C86636961125B1B9A2,
	AssetBundleRequest_get_asset_mB0A96FBC026D143638E467DEB37228ACD55F1813,
	AssetBundleRequest__ctor_mD09AF030644EF7F3386ABB3B5C593F61ADE25017,
};
static const int32_t s_InvokerIndices[6] = 
{
	-1,
	732,
	732,
	1920,
	1920,
	1969,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000001, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)1, 52 },
};
extern const CustomAttributesCacheGenerator g_UnityEngine_AssetBundleModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_AssetBundleModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AssetBundleModule_CodeGenModule = 
{
	"UnityEngine.AssetBundleModule.dll",
	6,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
	g_UnityEngine_AssetBundleModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
